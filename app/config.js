const app = angular.module("app", [
  "oc.lazyLoad",
  "ui.router",
  "angularCSS",
  "ngFileUpload",
  "uiCropper"
]);

//firebase auth
firebase.initializeApp({
  apiKey: "AIzaSyAGEHpaBX7SrOA2Gb1OBTgQJaWG-5r10lU",
  authDomain: "playout-online.firebaseapp.com",
  databaseURL: "https://playout-online.firebaseio.com",
  projectId: "playout-online",
  storageBucket: "playout-online.appspot.com",
  messagingSenderId: "422965033208"
});

app.filter("bytes", () => {
  return function(bytes) {
    let kb = bytes / 1024;
    let mb = kb / 1024;
    let gb = mb / 1024;
    let res;

    if (kb <= 1024) {
      res = Math.ceil(kb) + "KB";
    } else if (kb > 1024 && mb <= 1024) {
      res = Math.round(mb * 100) / 100 + "MB";
    } else if (mb > 1024) {
      res = Math.round(gb * 100) / 100 + "GB";
    }

    return res;
  };
});

app.directive("ngEnter", () => {
  return (scope, element, attrs) => {
    element.bind("keydown keypress", event => {
      if (event.which === 13) {
        scope.$apply(() => {
          scope.$eval(attrs.ngEnter);
        });
        event.preventDefault();
      }
    });
  };
});
