angular
  .module("app")
  .controller("loginController", ($scope, server) => {
    info("Login loaded");

    $scope.logIn = form => {
      server.in(`${form.user}@playout.online`, form.pass);
    };

    $scope.hidePass = true;

    $scope.switchPass = () => {
      let pass = document.getElementById("pass");
      $scope.hidePass = $scope.hidePass ? false : true;

      if (pass.type === "password") {
        pass.type = "text";
      } else if (pass.type === "text") {
        pass.type = "password";
      }
    };
  });
