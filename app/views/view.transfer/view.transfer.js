angular
  .module("app")
  .controller(
    "transferController",
    ($scope, server, progress, $timeout, $state) => {
      info("Transfer loaded");

      $scope.modulesInfo = "Playout v1.2";
      $scope.currentState = $state.current.name;
      $scope.progress = progress;
      $scope.Math = window.Math;

      function prettyBytes(num) {
        var exponent,
          unit,
          neg = num < 0,
          units = ["B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
        if (neg) num = -num;
        if (num < 1) return (neg ? "-" : "") + num + " B";
        exponent = Math.min(
          Math.floor(Math.log(num) / Math.log(1000)),
          units.length - 1
        );
        num = Number((num / Math.pow(1000, exponent)).toFixed(2));
        unit = units[exponent];
        return (neg ? "-" : "") + num + " " + unit;
      }

      let input = document.getElementById("file");
      let client = new WebTorrent();
      let transferRef = server.db.collection($scope.currentState);

      transferRef.get().then(snap => {
        $scope.files = [];
        snap.forEach(doc => {
          $scope.$apply(() => {
            $scope.files.push(doc.data());
          });
        });
      });

      $scope.sendFile = () => {
        if (input.files[0] !== undefined) {
          progress.start("Preparando el archivo");
          client.seed(
            input.files[0],
            { name: input.files[0].name },
            torrent => {
              let file = {
                name: torrent.name,
                magnetURI: torrent.magnetURI,
                length: torrent.length
              };

              transferRef.add(file).then(doc => {
                transferRef
                  .doc(doc.id)
                  .update({
                    ref: doc.id
                  })
                  .then(() => {
                    $timeout(() => progress.finish());
                    input.value = null;
                    $state.reload();
                  });
              });
            }
          );
        }
      };

      $scope.delete = file => {
        transferRef
          .doc(file.ref)
          .delete()
          .then(() => {
            $state.reload();
            $timeout(() => progress.finish());
          });

        client.remove(file.magnetURI, err => {
          if (err) {
            alert(err);
          }
        });
      };

      client.on("error", err => {
        $timeout(() => progress.finish());
        input.value = null;
      });

      $scope.downloadFile = (file, e) => {
        let span =
          e.currentTarget.parentNode.parentNode.childNodes[3].childNodes[3];
        let magnet = file.magnetURI;
        client.torrents.forEach(torrent => {
          if (torrent === magnet) {
            client.remove(magnet);
          }
        });
        span.innerText = "Preparando...";

        client.add(magnet, torrent => {
          torrent.on("download", function(bytes) {
            span.innerText = `Velocidad: ${prettyBytes(
              torrent.downloadSpeed
            )} - ${prettyBytes(torrent.downloaded)}/${prettyBytes(
              torrent.length
            )} - ${moment
              .duration(torrent.timeRemaining / 1000, "seconds")
              .locale("es")
              .humanize()}`;
          });

          torrent.files[0].getBlobURL(function(err, url) {
            if (err) throw err;
            let a = document.createElement("a");
            a.download = torrent.files[0].name;
            a.href = url;
            a.click();
            span.innerText = prettyBytes(torrent.length);
          });
        });

        $timeout(() => {
          client.torrents.forEach(torrent => {
            if (torrent.magnetURI === magnet && torrent.files.length === 0) {
              alert("Este archivo se ha dejado de compartir, y sera eliminado");
              $scope.delete(file);
            }
          });
        }, 5000);
      };
    }
  );
