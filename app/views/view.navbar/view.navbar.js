angular
  .module("app")
  .controller("navController", ($scope, $state) => {
    info("Nav loaded");

    $scope.currentState = $state.current.name;
    $scope.reload = () => location.reload();
  });
