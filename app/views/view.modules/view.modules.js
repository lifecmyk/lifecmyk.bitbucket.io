angular
  .module("app")
  .controller("modulesController", ($scope, $stateParams) => {
    info("Modules loaded");

    $scope.modulesInfo = "Playout v1.2";
    $scope.activeModule = $stateParams.m;
    
    $scope.activeDb = true;
    $scope.activeCr = true;

    if (window.innerWidth <= 1024) {
      $scope.activeDb = true;
      $scope.activeCr = false;

      $scope.foldDb = () => {
        if (!$scope.activeDb) {
          $scope.activeDb = true;
          $scope.activeCr = false;
        }
      };

      $scope.foldCr = () => {
        if (!$scope.activeCr) {
          $scope.activeCr = true;
          $scope.activeDb = false;
        }
      };
    }
  });
