angular.module("app").controller("projectsController", ($scope, server) => {
  info("Projects loaded");

  let projectsRef = server.db
    .collection("dossiers")
    .where("web_state", "==", "online");

  $scope.projects = [];
  $scope.project;
  $scope.isOpen = false;

  projectsRef.get().then(snap => {
    snap.forEach(doc => {
      $scope.projects.push(doc.data());
    });
    $scope.$apply(() => $scope.projects);
  });
  
  $scope.open = id => {
    server.db.collection("dossiers")
    .doc(id)
    .get()
    .then(doc => {
      log(doc.data());
      $scope.project = doc.data();
      $scope.$apply(() => $scope.project);
      });

    $scope.isOpen = true;
  };

  $scope.close = () => {
    $scope.isOpen = false;
    $scope.project = null;
  };
});
