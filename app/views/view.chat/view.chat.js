angular
  .module("app")
  .controller("chatController", ($scope, $timeout, server) => {
    info("Chat loaded");

    function genID() {
      let start = new Date().valueOf();
      var count = 3;
      var i = 0;
      return start.toString().replace(/(\d{3})/g, function(match, capture) {
        return i++ < count ? capture + "-" : capture;
      });
    }

    let myId = genID();

    $scope.randomID = myId;
    const chatRef = server.db.collection("chat");
    const videoOut = document.getElementById("videoOut");
    const videoIn = document.getElementById("videoIn");
    const servers = {
      iceServers: [
        { urls: "stun:stun.services.mozilla.com" },
        { urls: "stun:stun.l.google.com:19302" },
        {
          urls: "turn:numb.viagenie.ca",
          credential: "webrtc",
          username: "websitebeaver@mail.com"
        }
      ]
    };
    let pc = new RTCPeerConnection(servers);

    function sendMessage(senderId, data) {
      chatRef
        .add({ sender: senderId, message: data })
        .then(doc => chatRef.doc(doc.id).delete());
    }

    function readMessage(data) {
      var msg = JSON.parse(data.message);
      var sender = data.sender;
      if (sender != myId) {
        if (msg.ice != undefined) {
          pc.addIceCandidate(new RTCIceCandidate(msg.ice));
        } else if (msg.sdp.type == "offer") {
          pc.setRemoteDescription(new RTCSessionDescription(msg.sdp))
            .then(() => pc.createAnswer())
            .then(answer => pc.setLocalDescription(answer))
            .then(() =>
              sendMessage(myId, JSON.stringify({ sdp: pc.localDescription }))
            );
        } else if (msg.sdp.type == "answer") {
          pc.setRemoteDescription(new RTCSessionDescription(msg.sdp));
        }
      }
    }

    chatRef.onSnapshot(snap => {
      let docs = [];
      snap.forEach(doc => {
        docs.push(doc.data());
      });
      readMessage(docs[0]);
    });

    $scope.copyNum = (num, e) => {
      navigator.clipboard.writeText(num).then(() => {
        log("Copied to clipboard");
        e.target.add("tooltip");
        $timeout(() => e.target.del("tooltip"), 500);
      });
    };

    $scope.startLocal = () => {
      navigator.mediaDevices
        .getUserMedia({ audio: true, video: true })
        .then(stream => (videoOut.srcObject = stream))
        .then(stream => {
          pc.addStream(stream);
          pc.onicecandidate = event => {
            event.candidate
              ? sendMessage(myId, JSON.stringify({ ice: event.candidate }))
              : console.log("Sent All Ice");
          };

          pc.onaddstream = event => (videoIn.srcObject = event.stream);
        });
    };

    $scope.startRemote = e => {
      pc.createOffer()
        .then(offer => pc.setLocalDescription(offer))
        .then(() =>
          sendMessage(myId, JSON.stringify({ sdp: pc.localDescription }))
        );
    };
  });
