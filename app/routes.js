app.config(($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) => {
  $ocLazyLoadProvider.config({
    debug: false,
    events: false
  });

  $urlRouterProvider.otherwise("/modules");

  $stateProvider.state("login", {
    name: "login",
    url: "/login",
    views: {
      content: {
        templateUrl: "./app/views/view.login/view.login.html",
        controller: "loginController"
      }
    },
    resolve: {
      redirect: (server, $state) => {
        server.auth.onAuthStateChanged(user => {
          if (user) {
            $state.go("modules");
          }
        });
      },
      loadCss: $css => {
        return $css.add([
          {
            href: "./assets/css/view.login.css"
          },
          {
            href: "./assets/css/view.m.login.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      contentCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/view.login.js");
      }
    }
  });

  $stateProvider.state("modules", {
    url: "/modules?:m:data",
    params: { m: null, data: null },
    views: {
      navbar: {
        templateUrl: "./app/views/view.navbar/view.navbar.html",
        controller: "navController"
      },
      content: {
        templateUrl: "./app/views/view.modules/view.modules.html",
        controller: "modulesController"
      }
    },
    resolve: {
      redirect: (server, $state) => {
        server.auth.onAuthStateChanged(user => {
          if (!user) {
            $state.go("login");
          }
        });
      },
      navCss: $css => {
        return $css.add([
          {
            href: "./assets/css/view.navbar.css"
          },
          {
            href: "./assets/css/view.m.navbar.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      navCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/view.navbar.js");
      },
      contentCss: $css => {
        return $css.add([
          {
            href: "./assets/css/view.modules.css"
          },
          {
            href: "./assets/css/view.m.modules.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      contentCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/view.modules.js");
      },
      topbarCss: $css => {
        return $css.add([
          {
            href: "./assets/css/component.topbar.css"
          },
          {
            href: "./assets/css/component.m.topbar.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      topbarCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/component.topbar.js");
      },
      modulesCardCss: $css => {
        return $css.add([
          {
            href: "./assets/css/component.modules-list.css"
          },
          {
            href: "./assets/css/component.m.modules-list.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      modulesCardCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/component.modules-list.js");
      },
      trabajadoresCss: $css => {
        return $css.add([
          {
            href: "./assets/css/module.trabajadores.css"
          },
          {
            href: "./assets/css/module.m.trabajadores.css",
            media: "screen and (max-width : 1024px)"
          },
          {
            href: "./assets/css/form.trabajadores.css"
          },
          {
            href: "./assets/css/form.m.trabajadores.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      trabajadoresCtrl: $ocLazyLoad => {
        return [
          $ocLazyLoad.load("./assets/js/module.trabajadores.js"),
          $ocLazyLoad.load("./assets/js/form.trabajadores.js"),
          $ocLazyLoad.load("./assets/js/print.trabajadores.js")
        ];
      },
      dossierCss: $css => {
        return $css.add([
          {
            href: "./assets/css/module.dossier.css"
          },
          {
            href: "./assets/css/module.m.dossier.css",
            media: "screen and (max-width : 1024px)"
          },
          {
            href: "./assets/css/form.dossier.css"
          },
          {
            href: "./assets/css/form.m.dossier.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      dossierCtrl: $ocLazyLoad => {
        return [
          $ocLazyLoad.load("./assets/js/module.dossier.js"),
          $ocLazyLoad.load("./assets/js/form.dossier.js"),
          $ocLazyLoad.load("./assets/js/print.dossier.js")
        ];
      },
      gestionCss: $css => {
        return $css.add([
          {
            href: "./assets/css/module.gestion.css"
          },
          {
            href: "./assets/css/module.m.gestion.css",
            media: "screen and (max-width : 1024px)"
          },
          {
            href: "./assets/css/form.gestion.css"
          },
          {
            href: "./assets/css/form.m.gestion.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      gestionCtrl: $ocLazyLoad => {
        return [
          $ocLazyLoad.load("./assets/js/module.gestion.js"),
          $ocLazyLoad.load("./assets/js/form.gestion.js"),
          $ocLazyLoad.load("./assets/js/print.gestion.js")
        ];
      },
      videoCss: $css => {
        return $css.add([
          {
            href: "./assets/css/module.video.css"
          },
          {
            href: "./assets/css/module.m.video.css",
            media: "screen and (max-width : 1024px)"
          },
          {
            href: "./assets/css/form.video.css"
          },
          {
            href: "./assets/css/form.m.video.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      videoCtrl: $ocLazyLoad => {
        return [
          $ocLazyLoad.load("./assets/js/module.video.js"),
          $ocLazyLoad.load("./assets/js/form.video.js"),
          $ocLazyLoad.load("./assets/js/print.video.js")
        ];
      }
    }
  });
  
  $stateProvider.state("transfer", {
    url: "/transfer",
    views: {
      navbar: {
        templateUrl: "./app/views/view.navbar/view.navbar.html",
        controller: "navController"
      },
      content: {
        templateUrl: "./app/views/view.transfer/view.transfer.html",
        controller: "transferController"
      }
    },
    resolve: {
      redirect: (server, $state) => {
        server.auth.onAuthStateChanged(user => {
          if (!user) {
            $state.go("login");
          }
        });
      },
      navCss: $css => {
        return $css.add([
          {
            href: "./assets/css/view.navbar.css"
          },
          {
            href: "./assets/css/view.m.navbar.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      navCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/view.navbar.js");
      },
      contentCss: $css => {
        return $css.add([
          {
            href: "./assets/css/view.transfer.css"
          },
          {
            href: "./assets/css/view.m.transfer.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      contentCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/view.transfer.js");
      },
      topbarCss: $css => {
        return $css.add([
          {
            href: "./assets/css/component.topbar.css"
          },
          {
            href: "./assets/css/component.m.topbar.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      topbarCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/component.topbar.js");
      },
    }
  });

  $stateProvider.state("chat", {
    url: "/chat",
    views: {
      navbar: {
        templateUrl: "./app/views/view.navbar/view.navbar.html",
        controller: "navController"
      },
      content: {
        templateUrl: "./app/views/view.chat/view.chat.html",
        controller: "chatController"
      }
    },
    resolve: {
      redirect: (server, $state) => {
        server.auth.onAuthStateChanged(user => {
          if (!user) {
            $state.go("login");
          }
        });
      },
      navCss: $css => {
        return $css.add([
          {
            href: "./assets/css/view.navbar.css"
          },
          {
            href: "./assets/css/view.m.navbar.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      navCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/view.navbar.js");
      },
      contentCss: $css => {
        return $css.add([
          {
            href: "./assets/css/view.chat.css"
          },
          {
            href: "./assets/css/view.m.chat.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      contentCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/view.chat.js");
      },
      topbarCss: $css => {
        return $css.add([
          {
            href: "./assets/css/component.topbar.css"
          },
          {
            href: "./assets/css/component.m.topbar.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      topbarCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/component.topbar.js");
      },
    }
  });

  $stateProvider.state("options", {
    url: "/options",
    views: {
      navbar: {
        templateUrl: "./app/views/view.navbar/view.navbar.html",
        controller: "navController"
      },
      content: {
        templateUrl: "./app/views/view.options/view.options.html",
        controller: "optionsController"
      }
    },
    resolve: {
      redirect: (server, $state) => {
        server.auth.onAuthStateChanged(user => {
          if (!user) {
            $state.go("login");
          }
        });
      },
      navCss: $css => {
        return $css.add([
          {
            href: "./assets/css/view.navbar.css"
          },
          {
            href: "./assets/css/view.m.navbar.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      navCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/view.navbar.js");
      },
      contentCss: $css => {
        return $css.add([
          {
            href: "./assets/css/view.options.css"
          },
          {
            href: "./assets/css/view.m.options.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      contentCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/view.options.js");
      },
      topbarCss: $css => {
        return $css.add([
          {
            href: "./assets/css/component.topbar.css"
          },
          {
            href: "./assets/css/component.m.topbar.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      topbarCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/component.topbar.js");
      },
    }
  });

  $stateProvider.state("contact", {
    url: "/contact",
    views: {
      navbar: {
        templateUrl: "./app/views/view.navbar/view.navbar.html",
        controller: "navController"
      },
      content: {
        templateUrl: "./app/views/view.contact/view.contact.html",
        controller: "contactController"
      }
    },
    resolve: {
      redirect: (server, $state) => {
        server.auth.onAuthStateChanged(user => {
          if (!user) {
            $state.go("login");
          }
        });
      },
      navCss: $css => {
        return $css.add([
          {
            href: "./assets/css/view.navbar.css"
          },
          {
            href: "./assets/css/view.m.navbar.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      navCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/view.navbar.js");
      },
      contentCss: $css => {
        return $css.add([
          {
            href: "./assets/css/view.contact.css"
          },
          {
            href: "./assets/css/view.m.contact.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      contentCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/view.contact.js");
      },
      topbarCss: $css => {
        return $css.add([
          {
            href: "./assets/css/component.topbar.css"
          },
          {
            href: "./assets/css/component.m.topbar.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      topbarCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/component.topbar.js");
      },
    }
  });

  $stateProvider.state("projects", {
    name: "projects",
    url: "/projects",
    views: {
      content: {
        templateUrl: "./app/views/view.projects/view.projects.html",
        controller: "projectsController"
      }
    },
    resolve: {
      loadCss: $css => {
        return $css.add([
          {
            href: "./assets/css/view.projects.css"
          },
          {
            href: "./assets/css/view.m.projects.css",
            media: "screen and (max-width : 1024px)"
          }
        ]);
      },
      contentCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/view.projects.js");
      }
    }
  });
});
