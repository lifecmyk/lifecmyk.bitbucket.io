angular.module("app").component("cTopbar", {
  templateUrl: "./app/components/component.topbar/component.topbar.html",
  controller: ($scope, $state, server, $stateParams) => {
    info("Topbar component loaded");

    $scope.currentModule = $stateParams.m === undefined ? null : $stateParams.m;
    $scope.currentState = $state.current.name;

    $scope.goBack = () => {
      if ($stateParams.m.includes("form-")) {
        $state.go("modules", {
          m: $stateParams.m.replace("form-", ""),
          data: null
        });
      } else {
        $state.go("modules", { m: null, data: null });
      }
    };

    $scope.logOut = () => {
      const r = confirm("¿Cerrar sesión?");
      if (r == true) {
        server.out();
        $state.go("login");
      }
    };

    //current state
    if ($scope.currentState === "modules") {
      if ($scope.currentModule !== null) {
        $scope.currentState = "Módulo";
      } else {
        $scope.currentState = "Módulos";
      }
    }
    if ($scope.currentState === "transfer") {
      $scope.currentState = "Transferencia";
    }
    if ($scope.currentState === "options") {
      $scope.currentState = "Opciones";
    }
    if ($scope.currentState === "contact") {
      $scope.currentState = "Contacto";
    }

    //current module

    //db
    if ($scope.currentModule === "trabajadores") {
      $scope.currentModule = "Trabajadores";
    }
    if ($scope.currentModule === "form-trabajadores") {
      $scope.currentModule = "Formulario trabajadores";
    }
    if ($scope.currentModule === "facturas") {
      $scope.currentModule = "Facturas";
    }
    if ($scope.currentModule === "form-facturas") {
      $scope.currentModule = "Formulario facturas";
    }
    if ($scope.currentModule === "presupuestos") {
      $scope.currentModule = "Presupuestos";
    }
    if ($scope.currentModule === "form-presupuestos") {
      $scope.currentModule = "Formulario presupuestos";
    }

    //cr
    if ($scope.currentModule === "dossiers") {
      $scope.currentModule = "Dossiers";
    }
    if ($scope.currentModule === "form-dossiers") {
      $scope.currentModule = "Formulario dossiers";
    }
    if ($scope.currentModule === "logotipe") {
      $scope.currentModule = "Logotipo";
    }

    const userRef = server.db.collection("user").doc("cmyk");

    userRef.get().then(doc => {
      $scope.currentUser = doc.data();
      $scope.$apply(() => {
        $scope.currentUser;
      });
    });
  }
});
