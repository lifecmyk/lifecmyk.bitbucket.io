angular.module("app").component("cModulesList", {
  templateUrl:
    "./app/components/component.modules-list/component.modules-list.html",
  bindings: {
    list: "<"
  },
  controller: ($scope, server, $timeout, $state) => {
    info("Modules list component loaded");

    $scope.goTo = item => {
      $state.go("modules", { m: item.ref.id });
    };

    const dbList = [
      {
        ref: server.db.collection("trabajadores"),
        name: "Trabajadores",
        size: 0
      },
      {
        ref: server.db.collection("facturas"),
        name: "Facturas",
        size: 0
      },
      {
        ref: server.db.collection("presupuestos"),
        name: "Presupuestos",
        size: 0
      }
    ];

    const crList = [
      {
        ref: server.db.collection("dossiers"),
        name: "Dossiers",
        size: 0
      },
      {
        ref: server.db.collection("logotipe"),
        name: "Logotipo",
        size: 0
      }
    ];

    $scope.searchDb = false;
    $scope.searchCrea = false;

    $scope.getSearchDb = () => {
      $scope.searchDb = !$scope.searchDb;
      $timeout(() => _(".input-db").focus());
    };

    $scope.getSearchCrea = () => {
      $scope.searchCrea = !$scope.searchCrea;
      $timeout(() => _(".input-crea").focus());
    };

    $scope.ifSearchDb = () => {
      if (null || "" === _(".input-db").value) {
        $scope.searchDb = !$scope.searchDb;
      }
    };

    $scope.ifSearchCrea = () => {
      if (null || "" === _(".input-crea").value) {
        $scope.searchCrea = !$scope.searchCrea;
      }
    };

    $timeout(() => {
      let list = $scope.$ctrl.list;

      if (list === "db") {
        $scope.moduleTitle = "Productividad";
      }

      if (list === "cr") {
        $scope.moduleTitle = "Diseño";
      }

      for (let i = 0; i < dbList.length; i++) {
        dbList[i].ref.get().then(snap => {
          dbList[i].size = snap.size;
          $scope.$apply(() => $scope.list);
        });
      }
      for (let i = 0; i < crList.length; i++) {
        crList[i].ref.get().then(snap => {
          crList[i].size = snap.size;
          $scope.$apply(() => $scope.list);
        });
      }

      if (list === "db") {
        $scope.list = dbList;
      }
      if (list === "cr") {
        $scope.list = crList;
      }
    });
  }
});
