angular.module("app").component("fDossier", {
  templateUrl: "./app/modules/module.dossier/form.dossier.html",
  controller: (
    $scope,
    server,
    $stateParams,
    $state,
    progress,
    $timeout,
    pdf,
    $http,
    base64
  ) => {
    info("Form dossier loaded");

    $scope.currentModule = $stateParams.m;
    $scope.parentModule = $scope.currentModule.replace("form-", "");
    $scope.currentData = $stateParams.data;
    $scope.progress = progress;
    $scope.newPage = {};

    //page types
    $scope.pageType = [
      {
        ref: "full-img",
        name: "1 IMÁGEN"
      },
      {
        ref: "left-txt-img",
        name: "TEXTO IZQ. E IMÁGEN"
      },
      {
        ref: "right-txt-img",
        name: "TEXTO DER. E IMÁGEN"
      },
      {
        ref: "full-txt",
        name: "SOLO TEXTO"
      },
      {
        ref: "dbl-img",
        name: "2 IMÁGENES"
      },
      {
        ref: "full-hphoto",
        name: "1 FOTOGRAFÍA HOR."
      },
      {
        ref: "dbl-vphoto",
        name: "2 FOTOGRAFÍAS VER."
      },
      {
        ref: "full-vphoto",
        name: "1 FOTOGRAFÍA VER."
      },
      {
        ref: "left-txt-hphoto",
        name: "TEXTO IZQ. Y FOTOGRAFÍA HOR."
      },
      {
        ref: "right-txt-hphoto",
        name: "TEXTO DER. Y FOTOGRAFÍA HOR."
      },
      {
        ref: "left-txt-vphoto",
        name: "TEXTO IZQ. Y FOTOGRAFÍA VER."
      },
      {
        ref: "right-txt-vphoto",
        name: "TEXTO DER. Y FOTOGRAFÍA VER."
      }
    ];

    //define playout modules relation
    $scope.modules = ["dossier"];

    if ($scope.currentData === null) {
      //add created data
      let now = new Date();

      $scope.form = {
        _created: now,
        modules: $scope.modules,
        web_state: "offline"
      };

      // define pages array if new dossier
      $scope.form.pages = [];
    } else {
      let formRef = server.db
        .collection($scope.parentModule)
        .doc($scope.currentData);

      formRef.get().then(doc => {
        $scope.$apply(() => {
          $scope.form = doc.data();
        });
      });
    }

    $scope.demandImg = false;
    $scope.showImg = () => {
      $scope.demandImg = true;
    };

    // full-img
    let originalFullImg;
    $scope.croppedFullImg;
    $scope.croppingFullImg = false;

    // left-txt-img
    let originalLeftTxtImg;
    $scope.croppedLeftTxtImg;
    $scope.croppingLeftTxtImg = false;

    // right-txt-img
    let originalRightTxtImg;
    $scope.croppedRightTxtImg;
    $scope.croppingRightTxtImg = false;

    // dbl-img-left
    let originalDblImgLeft;
    $scope.croppedDblImgLeft;
    $scope.croppingDblImgLeft = false;

    // dbl-img-right
    let originalDblImgRight;
    $scope.croppedDblImgRight;
    $scope.croppingDblImgRight = false;

    // full-hphoto
    let originalFullHphoto;
    $scope.croppedFullHphoto;
    $scope.croppingFullHphoto = false;

    // dbl-vphoto-left
    let originalDblVphotoLeft;
    $scope.croppedDblVphotoLeft;
    $scope.croppingDblVphotoLeft = false;

    // dbl-vphoto-right
    let originalDblVphotoRight;
    $scope.croppedDblVphotoRight;
    $scope.croppingDblVphotoRight = false;

    // full-vphoto
    let originalFullVphoto;
    $scope.croppedFullVphoto;
    $scope.croppingFullVphoto = false;

    // left-txt-hphoto
    let originalLeftTxtHphoto;
    $scope.croppedLeftTxtHphoto;
    $scope.croppingLeftTxtHphoto = false;

    // right-txt-hphoto
    let originalRightTxtHphoto;
    $scope.croppedRightTxtHphoto;
    $scope.croppingRightTxtHphoto = false;

    // left-txt-vphoto
    let originalLeftTxtVphoto;
    $scope.croppedLeftTxtVphoto;
    $scope.croppingLeftTxtVphoto = false;

    // right-txt-vphoto
    let originalRightTxtVphoto;
    $scope.croppedRightTxtVphoto;
    $scope.croppingRightTxtVphoto = false;

    // full-img / crop functions
    $scope.cropFullImg = img => {
      if (img.name === "cropped.jpg") {
        $scope.newPage.images[0] = originalFullImg;
      } else {
        originalFullImg = img;
      }
      $scope.croppingFullImg = true;
    };
    $scope.applyCropFullImg = cropped => {
      $scope.newPage.images[0] = base64.toFile(cropped, "cropped.jpg");
      $scope.croppingFullImg = false;
    };

    //left-txt-img crop functions
    $scope.cropLeftTxtImg = img => {
      if (img.name === "cropped.jpg") {
        $scope.newPage.images[0] = originalLeftTxtImg;
      } else {
        originalLeftTxtImg = img;
      }
      $scope.croppingLeftTxtImg = true;
    };
    $scope.applyCropLeftTxtImg = cropped => {
      $scope.newPage.images[0] = base64.toFile(cropped, "cropped.jpg");
      $scope.croppingLeftTxtImg = false;
    };

    //right-txt-img crop functions
    $scope.cropRightTxtImg = img => {
      if (img.name === "cropped.jpg") {
        $scope.newPage.images[0] = originalRightTxtImg;
      } else {
        originalRightTxtImg = img;
      }
      $scope.croppingRightTxtImg = true;
    };
    $scope.applyCropRightTxtImg = cropped => {
      $scope.newPage.images[0] = base64.toFile(cropped, "cropped.jpg");
      $scope.croppingRightTxtImg = false;
    };

    //dbl-img / crop functions
    $scope.cropDblImgLeft = img => {
      if (img.name === "cropped-left.jpg") {
        $scope.newPage.images[0] = originalDblImgLeft;
      } else {
        originalDblImgLeft = img;
      }
      $scope.croppingDblImgLeft = true;
    };
    $scope.applyCropDblImgLeft = cropped => {
      $scope.newPage.images[0] = base64.toFile(cropped, "cropped-left.jpg");
      $scope.croppingDblImgLeft = false;
    };
    $scope.cropDblImgRight = img => {
      if (img.name === "cropped-right.jpg") {
        $scope.newPage.images[1] = originalDblImgRight;
      } else {
        originalDblImgRight = img;
      }
      $scope.croppingDblImgRight = true;
    };
    $scope.applyCropDblImgRight = cropped => {
      $scope.newPage.images[1] = base64.toFile(cropped, "cropped-right.jpg");
      $scope.croppingDblImgRight = false;
    };

    // full-hphoto / crop functions
    $scope.cropFullHphoto = img => {
      if (img.name === "cropped.jpg") {
        $scope.newPage.images[0] = originalFullHphoto;
      } else {
        originalFullHphoto = img;
      }
      $scope.croppingFullHphoto = true;
    };
    $scope.applyCropFullHphoto = cropped => {
      $scope.newPage.images[0] = base64.toFile(cropped, "cropped.jpg");
      $scope.croppingFullHphoto = false;
    };

    //dbl-vphoto / crop functions
    $scope.cropDblVphotoLeft = img => {
      if (img.name === "cropped-left.jpg") {
        $scope.newPage.images[0] = originalDblVphotoLeft;
      } else {
        originalDblVphotoLeft = img;
      }
      $scope.croppingDblVphotoLeft = true;
    };
    $scope.applyCropDblVphotoLeft = cropped => {
      $scope.newPage.images[0] = base64.toFile(cropped, "cropped-left.jpg");
      $scope.croppingDblVphotoLeft = false;
    };
    $scope.cropDblVphotoRight = img => {
      if (img.name === "cropped-right.jpg") {
        $scope.newPage.images[1] = originalDblVphotoRight;
      } else {
        originalDblVphotoRight = img;
      }
      $scope.croppingDblVphotoRight = true;
    };
    $scope.applyCropDblVphotoRight = cropped => {
      $scope.newPage.images[1] = base64.toFile(cropped, "cropped-right.jpg");
      $scope.croppingDblVphotoRight = false;
    };

    // full-hphoto / crop functions
    $scope.cropFullVphoto = img => {
      if (img.name === "cropped.jpg") {
        $scope.newPage.images[0] = originalFullVphoto;
      } else {
        originalFullVphoto = img;
      }
      $scope.croppingFullVphoto = true;
    };
    $scope.applyCropFullVphoto = cropped => {
      $scope.newPage.images[0] = base64.toFile(cropped, "cropped.jpg");
      $scope.croppingFullVphoto = false;
    };

    //left-txt-hphoto crop functions
    $scope.cropLeftTxtHphoto = img => {
      if (img.name === "cropped.jpg") {
        $scope.newPage.images[0] = originalLeftTxtHphoto;
      } else {
        originalLeftTxtHphoto = img;
      }
      $scope.croppingLeftTxtHphoto = true;
    };
    $scope.applyCropLeftTxtHphoto = cropped => {
      $scope.newPage.images[0] = base64.toFile(cropped, "cropped.jpg");
      $scope.croppingLeftTxtHphoto = false;
    };

    //right-txt-hphoto crop functions
    $scope.cropRightTxtHphoto = img => {
      if (img.name === "cropped.jpg") {
        $scope.newPage.images[0] = originalRightTxtHphoto;
      } else {
        originalRightTxtHphoto = img;
      }
      $scope.croppingRightTxtHphoto = true;
    };
    $scope.applyCropRightTxtHphoto = cropped => {
      $scope.newPage.images[0] = base64.toFile(cropped, "cropped.jpg");
      $scope.croppingRightTxtHphoto = false;
    };

    //left-txt-hphoto crop functions
    $scope.cropLeftTxtVphoto = img => {
      if (img.name === "cropped.jpg") {
        $scope.newPage.images[0] = originalLeftTxtVphoto;
      } else {
        originalLeftTxtVphoto = img;
      }
      $scope.croppingLeftTxtVphoto = true;
    };
    $scope.applyCropLeftTxtVphoto = cropped => {
      $scope.newPage.images[0] = base64.toFile(cropped, "cropped.jpg");
      $scope.croppingLeftTxtVphoto = false;
    };

    //right-txt-hphoto crop functions
    $scope.cropRightTxtVphoto = img => {
      if (img.name === "cropped.jpg") {
        $scope.newPage.images[0] = originalRightTxtVphoto;
      } else {
        originalRightTxtVphoto = img;
      }
      $scope.croppingRightTxtVphoto = true;
    };
    $scope.applyCropRightTxtVphoto = cropped => {
      $scope.newPage.images[0] = base64.toFile(cropped, "cropped.jpg");
      $scope.croppingRightTxtVphoto = false;
    };

    //start new page
    $scope.startPage = page => {
      $scope.newPage.images = [];
      $scope.newPageFullImg = false;
      $scope.newPageLeftTxtImg = false;
      $scope.newPageRightTxtImg = false;
      $scope.newPageFullTxt = false;
      $scope.newPageDblImg = false;
      $scope.newPageFullHphoto = false;
      $scope.newPageDblVphoto = false;
      $scope.newPageFullVphoto = false;
      $scope.newPageLeftTxtHphoto = false;
      $scope.newPageRightTxtHphoto = false;
      $scope.newPageLeftTxtVphoto = false;
      $scope.newPageRightTxtVphoto = false;

      page === "full-img"
        ? ($scope.newPageFullImg = true)
        : ($scope.newPageFullImg = false);
      page === "left-txt-img"
        ? ($scope.newPageLeftTxtImg = true)
        : ($scope.newPageLeftTxtImg = false);
      page === "right-txt-img"
        ? ($scope.newPageRightTxtImg = true)
        : ($scope.newPageRightTxtImg = false);
      page === "full-txt"
        ? ($scope.newPageFullTxt = true)
        : ($scope.newPageFullTxt = false);
      page === "dbl-img"
        ? ($scope.newPageDblImg = true)
        : ($scope.newPageDblImg = false);
      page === "full-hphoto"
        ? ($scope.newPageFullHphoto = true)
        : ($scope.newPageFullHphoto = false);
      page === "dbl-vphoto"
        ? ($scope.newPageDblVphoto = true)
        : ($scope.newPageDblVphoto = false);
      page === "full-vphoto"
        ? ($scope.newPageFullVphoto = true)
        : ($scope.newPageFullVphoto = false);
      page === "left-txt-hphoto"
        ? ($scope.newPageLeftTxtHphoto = true)
        : ($scope.newPageLeftTxtHphoto = false);
      page === "right-txt-hphoto"
        ? ($scope.newPageRightTxtHphoto = true)
        : ($scope.newPageRightTxtHphoto = false);
      page === "left-txt-vphoto"
        ? ($scope.newPageLeftTxtVphoto = true)
        : ($scope.newPageLeftTxtVphoto = false);
      page === "right-txt-vphoto"
        ? ($scope.newPageRightTxtVphoto = true)
        : ($scope.newPageRightTxtVphoto = false);
    };

    //dicard new page
    $scope.discardPage = () => {
      $scope.newPageFullImg = false;
      $scope.newPageLeftTxtImg = false;
      $scope.newPageRightTxtImg = false;
      $scope.newPageFullTxt = false;
      $scope.newPageDblImg = false;
      $scope.newPageFullHphoto = false;
      $scope.newPageDblVphoto = false;
      $scope.newPageFullVphoto = false;
      $scope.newPageLeftTxtHphoto = false;
      $scope.newPageRightTxtHphoto = false;
      $scope.newPageLeftTxtVphoto = false;
      $scope.newPageRightTxtVphoto = false;
      $scope.newPage = {};
    };

    //add new page
    $scope.addPage = (d, t) => {
      let page = {
        type: t
      };

      let images;

      if (d.images) {
        images = Object.values(d.images);
        page.images = [];

        for (let i = 0; i < images.length; i++) {
          page.images.push(images[i]);
        }
      }

      if (d.title) {
        page.title = d.title;
      }

      if (d.text) {
        if (d.text.includes("\n")) {
          page.text = d.text.replace(/(\r\n|\n|\r)/gm, "");
        } else {
          page.text = d.text;
        }
      }

      $scope.form.pages.push(page);
      $scope.discardPage();
    };

    //delete new page
    $scope.deletePage = key => {
      $scope.form.pages.splice(key, 1);
    };

    //add new
    $scope.add = form => {
      if (!form.pages.length) {
        alert("No has añadido ninguna página");
      } else {
        progress.add(0);
        progress.start("Creando documento");

        //duplicate & keep form data
        let myForm = form;

        //def new var for pages
        let pages = form.pages;

        //def ref var
        let docId;

        //images counters
        let allImgs = [];
        let uploadedImgs = [];
        let onlyTxt = [];

        //crear form data to get docid
        form = {};

        //fake promise for uploaded images
        $scope.fakePromise = downloadURL => {
          uploadedImgs.push(downloadURL);

          if (uploadedImgs.length !== allImgs.length) {
            info(`Uploading image ${uploadedImgs.length} of ${allImgs.length}`);
            $timeout(() => {
              progress.show(
                `Subiendo imágen ${uploadedImgs.length} of ${allImgs.length}`
              );
              progress.add((uploadedImgs.length * 100) / allImgs.length - 5);
            });
          } else {
            myForm.ref = docId;

            $timeout(() => {
              progress.show("Documento subido");
              progress.add(100);
            });

            server.db
              .collection($scope.parentModule)
              .doc(docId)
              .set(myForm)
              .then(() => {
                succ("Form fully uploaed");
                progress.finish();

                $state.go("modules", {
                  m: $scope.parentModule,
                  data: null
                });
              });
          }
        };

        server.db
          .collection($scope.parentModule)
          .add(form)
          .then(docRef => {
            succ("Doc created");
            docId = docRef.id;

            //put every page in a scope variable
            for (let p = 0; p < pages.length; p++) {
              eval(`$scope.page${p} = pages[${p}]`);

              //check if page has images
              if (eval(`pages[${p}].images`)) {
                eval(`$scope.page${p}Images = pages[${p}].images`);

                //upload and def URL of every image
                for (let i = 0; i < eval(`$scope.page${p}Images.length`); i++) {
                  //safe all images to know total of images
                  eval(`$scope.page${p}Image${i} = {};`);
                  eval(`$scope.page${p}Image${i}`)[`page${p}Image${i}`] = eval(
                    `$scope.page${p}Images[${i}]`
                  );
                  allImgs.push(eval(`$scope.page${p}Image${i}`));

                  //start image upload & def image url
                  server.st
                    .ref()
                    .child(
                      `${$scope.parentModule}/${docId}/page${p}Image${i}.jpg`
                    )
                    .put(eval(`$scope.page${p}Images[${i}]`))
                    .then(snap => {
                      let progress =
                        (snap.bytesTransferred / snap.totalBytes) * 100;
                      info(`Upload is ${progress}% done`);

                      snap.ref.getDownloadURL().then(downloadURL => {
                        succ(`Image ${i} from page ${p} successfully added`);
                        eval(`myForm.pages[${p}].images[${i}] = downloadURL`);

                        //wait until all images are uploaded
                        $scope.fakePromise(downloadURL);
                      });
                    })
                    .catch(err => {
                      alert(
                        `Algo ha salido mal. Vulve a intentarlo o ponte en contacto con el desarrollador.\n\n${err}`
                      );
                      location.reload();
                    });
                }
              } else {
                onlyTxt.push(`page${p}`);

                if (pages.length === onlyTxt.length) {
                  $timeout(() => {
                    progress.show("Documento subido");
                    progress.add(100);
                  });

                  myForm.ref = docId;

                  server.db
                    .collection($scope.parentModule)
                    .doc(docId)
                    .set(myForm)
                    .then(() => {
                      succ("Form fully uploaed");
                      progress.finish();

                      $state.go("modules", {
                        m: $scope.parentModule,
                        data: null
                      });
                    })
                    .catch(err => {
                      alert(
                        `Algo ha salido mal. Vulve a intentarlo o ponte en contacto con el desarrollador.\n\n${err}`
                      );
                      location.reload();
                    });
                }
              }
            }
          });
      }
    };

    //update
    $scope.update = form => {
      if (!form.pages.length) {
        alert("No has añadido ninguna página");
      } else {
        progress.add(0);
        progress.start("Actualizando documento");

        //duplicate & keep form data
        let myForm = form;

        //def new var for pages
        let pages = form.pages;

        //def ref var
        let docId = myForm.ref;

        //images counters
        let allImgs = [];
        let uploadedImgs = [];
        let onlyTxt = [];
        let isObj = [];

        //fake promise for uploaded images
        $scope.fakePromise = downloadURL => {
          uploadedImgs.push(downloadURL);

          if (uploadedImgs.length !== allImgs.length) {
            info(`Uploading image ${uploadedImgs.length} of ${allImgs.length}`);
            $timeout(() => {
              progress.show(
                `Subiendo imágen ${uploadedImgs.length} of ${allImgs.length}`
              );
              progress.add((uploadedImgs.length * 100) / allImgs.length - 5);
            });
          } else {
            $timeout(() => {
              progress.show("Documento actualizado");
              progress.add(100);
            });

            server.db
              .collection($scope.parentModule)
              .doc(docId)
              .update(myForm)
              .then(() => {
                succ("Form fully uploaed");
                progress.finish();

                $state.go("modules", {
                  m: $scope.parentModule,
                  data: null
                });
              })
              .catch(err => {
                alert(
                  `Algo ha salido mal. Vulve a intentarlo o ponte en contacto con el desarrollador.\n\n${err}`
                );
                location.reload();
              });
          }
        };

        //put every page in a scope variable
        for (let p = 0; p < pages.length; p++) {
          eval(`$scope.page${p} = pages[${p}]`);

          //check if page has images
          if (eval(`pages[${p}].images`)) {
            eval(`$scope.page${p}Images = pages[${p}].images`);

            //upload and def URL of every image
            for (let i = 0; i < eval(`$scope.page${p}Images.length`); i++) {
              eval(`$scope.page${p}Image${i} = {};`);

              if (typeof eval(`$scope.page${p}Images[${i}]`) === "object") {
                //push all object to check if no one is object
                isObj.push(eval(`$scope.page${p}Images[${i}]`));

                //safe all images to know total of images
                eval(`$scope.page${p}Image${i}`)[`page${p}Image${i}`] = eval(
                  `$scope.page${p}Images[${i}]`
                );

                allImgs.push(eval(`$scope.page${p}Image${i}`));

                //start image upload & def image url
                server.st
                  .ref()
                  .child(
                    `${$scope.parentModule}/${docId}/page${p}Image${i}.jpg`
                  )
                  .put(eval(`$scope.page${p}Images[${i}]`))
                  .then(snap => {
                    let progress =
                      (snap.bytesTransferred / snap.totalBytes) * 100;
                    info(`Upload is ${progress}% done`);

                    snap.ref.getDownloadURL().then(downloadURL => {
                      succ(`Image ${i} from page ${p} successfully added`);
                      eval(`myForm.pages[${p}].images[${i}] = downloadURL`);

                      //wait until all images are uploaded
                      $scope.fakePromise(downloadURL);
                    });
                  })
                  .catch(err => {
                    alert(
                      `Algo ha salido mal. Vulve a intentarlo o ponte en contacto con el desarrollador.\n\n${err}`
                    );
                    location.reload();
                  });
              }
            }
          } else {
            onlyTxt.push(`page${p}`);

            if (pages.length === onlyTxt.length) {
              $timeout(() => {
                progress.show("Documento actualizado");
                progress.add(100);
              });

              server.db
                .collection($scope.parentModule)
                .doc(docId)
                .update(myForm)
                .then(() => {
                  succ("Form fully uploaed");
                  progress.finish();

                  $state.go("modules", {
                    m: $scope.parentModule,
                    data: null
                  });
                })
                .catch(err => {
                  alert(
                    `Algo ha salido mal. Vulve a intentarlo o ponte en contacto con el desarrollador.\n\n${err}`
                  );
                  location.reload();
                });
            }
          }
        }

        //in no new images just update
        if (isObj.length === 0) {
          $timeout(() => {
            progress.show("Documento actualizado");
            progress.add(100);
          });

          server.db
            .collection($scope.parentModule)
            .doc(docId)
            .update(myForm)
            .then(() => {
              succ("Form fully uploaed");
              progress.finish();

              $state.go("modules", {
                m: $scope.parentModule,
                data: null
              });
            })
            .catch(err => {
              alert(
                `Algo ha salido mal. Vulve a intentarlo o ponte en contacto con el desarrollador.\n\n${err}`
              );
              location.reload();
            });
        }
      }
    };

    $scope.checkPush = form => {
      if ($scope.currentData === null) {
        $scope.add(form);
      } else {
        $scope.update(form);
      }
    };

    app.printDossier($scope, pdf, $http, $timeout, progress);
  }
});
