angular.module("app").component("mDossier", {
  templateUrl: "./app/modules/module.dossier/module.dossier.html",
  controller: (
    $scope,
    server,
    $stateParams,
    $state,
    pdf,
    $http,
    $timeout,
    progress
  ) => {
    info("Module dossier loaded");
    log("Module dossier loaded");

    $scope.currentModule = $stateParams.m;
    $scope.moduleSelected = "dossier";
    $scope.progress = progress;

    let moduleRef = server.db.collection($scope.currentModule);

    moduleRef.get().then(snap => {
      $scope.moduleData = [];
      snap.forEach(doc => {
        $scope.$apply(() => {
          $scope.moduleData.push(doc.data());

          $scope.dossierFor = dossierFor => {
            return `./assets/templates/dossier/${dossierFor}/${dossierFor}.svg`;
          };
        });
      });
    });

    $scope.edit = item => {
      $state.go("modules", {
        m: `form-${$scope.currentModule}`,
        data: item.ref
      });
    };

    $scope.delete = item => {
      let r = confirm("¿Eliminar permanentemente?");
      progress.add(0);
      progress.start("Eliminando documento");

      if (r === true) {
        let pages = item.pages;
        let allImgs = [];
        let allDeleteds = [];
        let onlyTxt = [];

        $scope.fakePromise = name => {
          allDeleteds.push(name);

          if (allImgs.length !== allDeleteds.length) {
            info(`Deleting image ${allDeleteds.length} of ${allImgs.length}`);
            $timeout(() => {
              progress.show(
                `Eliminando imágen ${allDeleteds.length} of ${allImgs.length}`
              );
              progress.add((allDeleteds.length * 100) / allImgs.length - 5);
            });
          } else {
            $timeout(() => {
              progress.show("Documento eliminado");
              progress.add(100);
            });

            server.db
              .collection($scope.currentModule)
              .doc(item.ref)
              .delete()
              .then(() => {
                succ("Document successfully deleted");
                progress.finish();
                $state.reload();
              })
              .catch(err => {
                warn(`Error removing document: ${err}`);
                alert(
                  `Algo ha salido mal. Vulve a intentarlo o ponte en contacto con el desarrollador.\n\n${err}`
                );
                location.reload();
              });
          }
        };

        //get num of pages, type & images to delete images from storage
        for (let p = 0; p < pages.length; p++) {
          //discard no images pages
          if (pages[p].type !== "full-txt") {
            let images = pages[p].images;

            for (let i = 0; i < images.length; i++) {
              //safe all images to know total of images
              allImgs.push(`page${p}Image${i}`);

              //first delete all images
              server.st
                .ref()
                .child(
                  `${$scope.currentModule}/${item.ref}/page${p}Image${i}.jpg`
                )
                .delete()
                .then(() => {
                  succ(`Image ${i} from page ${p} deleted`);
                  $scope.fakePromise(`page${p}Image${i}`);
                })
                .catch(err => {
                  warn(`Error removing image: ${err}`);
                  alert(
                    `Algo ha salido mal. Vulve a intentarlo o ponte en contacto con el desarrollador.\n\n${err}`
                  );
                  location.reload();
                });
            }
          } else {
            onlyTxt.push(`page${p}`);

            if (pages.length === onlyTxt.length) {
              $timeout(() => {
                progress.show("Documento eliminado");
                progress.add(100);
              });

              server.db
                .collection($scope.currentModule)
                .doc(item.ref)
                .delete()
                .then(() => {
                  succ("Document successfully deleted");
                  progress.finish();

                  $state.reload();
                })
                .catch(err => {
                  warn(`Error removing document: ${err}`);
                  alert(
                    `Algo ha salido mal. Vulve a intentarlo o ponte en contacto con el desarrollador.\n\n${err}`
                  );
                  location.reload();
                });
            }
          }
        }
      }
    };

    app.printDossier($scope, pdf, $http, $timeout, progress);

    $scope.activePreview = false;

    $scope.preview = (item, moduleSelected) => {
      $scope.print(item, moduleSelected, true);
      $scope.activePreview = true;
    };

    $scope.closePreview = () => {
      let frame = document.querySelector("iframe");
      frame.classList.remove("dev-active");
      frame.src = "";
      $scope.activePreview = false;
    };
  }
});
