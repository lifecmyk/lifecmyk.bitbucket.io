angular.module("app").component("mTrabajadores", {
  templateUrl: "./app/modules/module.trabajadores/module.trabajadores.html",
  controller: ($scope, server, $stateParams, $state, pdf, $http, $timeout, progress) => {
    info("Module trabajadores loaded");

    $scope.currentModule = $stateParams.m;
    $scope.moduleSelected = "tarjetas";
    $scope.progress = progress;

    let moduleRef = server.db.collection($scope.currentModule);

    moduleRef.get().then(snap => {
      $scope.moduleData = [];
      snap.forEach(doc => {
        $scope.$apply(() => {
          $scope.moduleData.push(doc.data());
        });
      });
    });

    $scope.edit = item => {
      $state.go("modules", {
        m: `form-${$scope.currentModule}`,
        data: item.ref
      });
    };

    $scope.delete = item => {
      let r = confirm("¿Eliminar permanentemente?");

      if (r === true) {
        progress.add(0);
        progress.start("Eliminando documento");

        server.db
          .collection($scope.currentModule)
          .doc(item.ref)
          .delete()
          .then(() => {
            $timeout(() => {
              progress.show("Eliminando imágen");
              progress.add(50);
            });
            succ("Document successfully deleted");

            let imgRef = server.st
              .ref()
              .child(`${$scope.currentModule}/${item.ref}/img.jpg`);

            imgRef
              .delete()
              .then(() => {
                $timeout(() => {
                  progress.show("Documento eliminado");
                  progress.add(1000);
                });
                succ("Image successfully deleted");
                progress.finish();
                $state.reload();
              })
              .catch(err => {
                warn(`Error removing image: ${err}`);
              });
          })
          .catch(err => {
            warn(`Error removing document: ${err}`);
            alert(
              `Algo ha salido mal. Vulve a intentarlo o ponte en contacto con el desarrollador.\n\n${err}`
            );
            location.reload();
          });
      }
    };

    app.printTrabajadores($scope, pdf, $http, $timeout, progress);
  }
});
