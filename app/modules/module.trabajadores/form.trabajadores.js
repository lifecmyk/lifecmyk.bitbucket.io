angular.module("app").component("fTrabajadores", {
  templateUrl: "./app/modules/module.trabajadores/form.trabajadores.html",
  controller: (
    $scope,
    server,
    $stateParams,
    $state,
    progress,
    $timeout,
    pdf,
    $http,
    svg,
    base64
  ) => {
    info("Form trabajadores loaded");

    $scope.currentModule = $stateParams.m;
    $scope.parentModule = $scope.currentModule.replace("form-", "");
    $scope.currentData = $stateParams.data;
    $scope.progress = progress;

    //define playout modules relation
    $scope.modules = ["tarjetas", "credenciales"];
    $scope.months = [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre"
    ];

    // check new or open
    if ($scope.currentData === null) {
      //add created data
      let now = new Date();

      $scope.form = {
        _created: now,
        img: "./assets/img/avatar.jpg",
        modules: $scope.modules
      };

      _("#image").setAttribute("required", "required");
    } else {
      let formRef = server.db
        .collection($scope.parentModule)
        .doc($scope.currentData);

      formRef.get().then(doc => {
        $scope.$apply(() => {
          $scope.form = doc.data();
        });
      });
    }

    //cropper
    let original;
    $scope.croppedImg;
    $scope.cropping = false;

    $scope.cropImg = img => {
      if (img.name === "cropped.jpg") {
        $scope.form.img = original;
      } else {
        original = img;
      }
      $scope.cropping = true;
    };
    $scope.applyCrop = cropped => {
      $scope.form.img = base64.toFile(cropped, "cropped.jpg");
      $scope.cropping = false;
    };

    //QR
    $scope.getQR = form => {
      let month;
      let day = form.birth.day;

      for (let m in $scope.months) {
        if (form.birth.month === $scope.months[m]) {
          month = parseInt(m) + 1;
        }
      }

      let birthDay = `${form.birth.year}${
        month.toString().length === 1
          ? "0" + month.toString()
          : month.toString()
      }${day.toString().length === 1 ? "0" + day.toString() : day.toString()}`;

      let vCard = {
        version: "3.0",
        lastName: form.lastname,
        firstName: form.name,
        workPhone: form.phone,
        birthday: birthDay,
        role: form.position,
        workEmail: form.email
      };

      let qr = qrCode.createVCardQr(vCard, {
        typeElement: "createSvg",
        typeNumber: 9,
        cellSize: 1.7
      });

      $scope.form.qr = svg.toURL(qr);

      succ("QR created");
      //test qr
      /* log($scope.form)
      _("form").innerHTML = qrCode.createVCard(vCard); */
    };

    //add new
    $scope.add = form => {
      progress.add(0);
      progress.start("Creando documento");

      $scope.getQR(form);

      //duplicate & save image variable
      let img = form.img

      //clean form image
      form.img = null;

      server.db
        .collection($scope.parentModule)
        .add(form)
        .then(docRef => {
          succ("Data added");

          //def doc id
          let docId = docRef.id;

          // ref image folder
          let imgRef = server.st
            .ref()
            .child(`${$scope.parentModule}/${docId}/img.jpg`);

          let taskRef = imgRef.put(img);

          taskRef.on(
            "state_changed",
            snapshot => {
              $timeout(() => {
                progress.show("Subiendo imágen");
                progress.add(50);
              });

              let prog = Math.round(
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100
              );
              info(`Upload is ${prog}% done`);
            },
            err => {
              warn(err);
            },
            () => {
              taskRef.snapshot.ref.getDownloadURL().then(downloadURL => {
                succ("Image uploaded");

                $timeout(() => {
                  progress.show("Documento subido");
                  progress.add(100);
                });

                server.db
                  .collection($scope.parentModule)
                  .doc(docId)
                  .update({
                    ref: docId,
                    img: downloadURL
                  })
                  .then(() => {
                    succ("Ref & image URL addeds");
                    progress.finish();

                    $state.go("modules", {
                      m: $scope.parentModule,
                      data: null
                    });
                  });
              });
            }
          );
        })
        .catch(err => {
          warn(`Error writing document: ${err}`);
          alert(
            `Algo ha salido mal. Vulve a intentarlo o ponte en contacto con el desarrollador.\n\n${err}`
          );
          location.reload();
        });
    };

    //update
    $scope.update = form => {
      progress.add(0);
      progress.start("Actualizando documento");

      //duplicate & save image variable
      let img = form.img;

      // ref image folder
      let imgRef = server.st
        .ref()
        .child(`${$scope.parentModule}/${$scope.currentData}/img.jpg`);

      if (typeof img !== "string") {
        let taskRef = imgRef.put(img);

        taskRef.on(
          "state_changed",
          snapshot => {
            $timeout(() => {
              progress.show("Subiendo imágen");
              progress.add(50);
            });
            let prog = Math.round(
              (snapshot.bytesTransferred / snapshot.totalBytes) * 100
            );
            info(`Upload is ${prog}% done`);
          },
          err => {
            warn(err);
          },
          () => {
            taskRef.snapshot.ref.getDownloadURL().then(downloadURL => {
              $timeout(() => {
                progress.show("Documento actualizado");
                progress.add(100);
              });

              succ("Image uploaded");
              form.img = downloadURL;

              server.db
                .collection($scope.parentModule)
                .doc($scope.currentData)
                .set(form)
                .then(() => {
                  succ("Data updated");
                  progress.finish();

                  $state.go("modules", {
                    m: $scope.parentModule,
                    data: null
                  });
                })
                .catch(err => {
                  warn(`Error writing document: ${err}`);
                  alert(
                    `Algo ha salido mal. Vulve a intentarlo o ponte en contacto con el desarrollador.\n\n${err}`
                  );
                  location.reload();
                });
            });
          }
        );
      } else {
        $timeout(() => {
          progress.show("Documento actualizado");
          progress.add(100);
        });

        server.db
          .collection($scope.parentModule)
          .doc($scope.currentData)
          .set(form)
          .then(() => {
            succ("Data updated");
            progress.finish();

            $state.go("modules", {
              m: $scope.parentModule,
              data: null
            });
          })
          .catch(err => {
            warn(`Error writing document: ${err}`);
            alert(
              `Algo ha salido mal. Vulve a intentarlo o ponte en contacto con el desarrollador.\n\n${err}`
            );
            location.reload();
          });
      }
    };

    $scope.checkPush = form => {
      if ($scope.currentData === null) {
        $scope.add(form);
      } else {
        $scope.update(form);
      }
    };

    app.printTrabajadores($scope, pdf, $http, $timeout, progress);
  }
});
