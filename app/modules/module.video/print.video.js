app.printVideo = ($scope, pdf, $http, $timeout, progress) => {
  $scope.print = (item, m) => {
    if (m === "tarjetas") {
      info(`PDF starts with module: ${m}`);
      progress.add(0);
      progress.start("Creando PDF");

      //def pdf name
      let pdfName = `${item.name}_TARJETAS`;

      //milimeters function
      let mm = num => pdf.mm(num);

      //pdf options
      let doc = new PDFDocument({
        size: pdf.size.portrait.card,
        margin: pdf.margin.small,
        info: {
          Title: pdfName
        }
      });

      //generate blob from pdf
      let stream = doc.pipe(blobStream());

      // preload files
      async function asyncData() {
        progress.show("Cargando tipografías");
        progress.add(30);

        // load fonts
        const lora = await $http.get(
          "./assets/templates/tarjetas/Lora-Italic.ttf",
          {
            responseType: "arraybuffer"
          }
        );

        // register fonts
        doc.registerFont("lora", lora.data);

        progress.show("Tipografías cargadas");
        progress.add(60);

        // bleeds
        $scope.bleeds = await $http.get(pdf.bleeds.portrait.card);

        progress.show("Cargando imágenes");
        progress.add(90);

        // images & vectors
        $scope.cara = await $http.get("./assets/templates/tarjetas/cara.svg");
        $scope.dorso = await $http.get("./assets/templates/tarjetas/dorso.svg");

        $timeout(() => {
          progress.show("Imágenes cargadas");
          progress.add(100);
        });
      }

      // design pdf
      asyncData()
        .then(() => {
          doc.addSVG($scope.cara.data, pdf.margin.small, pdf.margin.small, {
            width: mm(61),
            height: mm(91)
          });

          doc.addSVG($scope.bleeds.data, 0, 0);

          doc.addPage();

          doc.addSVG($scope.dorso.data, pdf.margin.small, pdf.margin.small, {
            width: mm(61),
            height: mm(91)
          });

          doc
            .font("lora", 10)
            .fillColor("#fff")
            .text(item.phone, mm(15.7), mm(88.7) - 12, {
              width: mm(47),
              align: "center"
            })
            .text(item.email, {
              width: mm(47),
              align: "center"
            });

          doc.addSVG($scope.bleeds.data, 0, 0);

          // render pdf
          doc.end();
        })
        .catch(err => {
          alert(
            `Algo ha salido mal. Vulve a intentarlo o ponte en contacto con el desarrollador.\n\n${err}`
          );
          location.reload();
        });

      // get pdf
      stream.on("finish", () => {
        let blob = stream.toBlobURL("application/pdf");

        //is prod ? dev
        let prod = true;
        if (prod) {
          let link = document.createElement("a");
          link.href = blob;
          link.download = `${pdfName}.pdf`;
          link.click();
        } else {
          let frame = document.querySelector("iframe");
          frame.classList.add("dev-active");
          frame.src = blob;
        }

        info("PDF ends");
        progress.finish();

        $timeout(() => {
          window.URL.revokeObjectURL(blob);
        }, 2000);

        /* let buffer = stream.toBlob();

        function blobToDataURL(blob, callback) {
          var a = new FileReader();
          a.onload = function(e) {
            callback(e.target.result);
          };
          a.readAsDataURL(blob);
        }

        blobToDataURL(buffer, function(url) {
          log(url);
          let link = document.createElement("a");
          link.href = url;
          link.download = `${pdfName}.pdf`;
          link.click();
        }); */

        /* let file = new File([buffer], `${pdfName}.pdf`);
        log(file); */
      });
    } else {
      alert(`El modulo ${m} no esta disponible`);
    }
  };
};
