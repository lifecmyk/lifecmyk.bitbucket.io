/* navigator.mediaDevices
        .enumerateDevices()
        .then(gotDevices)
        .then(getStream)
        .catch(handleError);

      audioId.onchange = getStream;
      videoId.onchange = getStream;

      function gotDevices(deviceInfos) {
        for (let i = 0; i !== deviceInfos.length; ++i) {
          const deviceInfo = deviceInfos[i];
          const option = document.createElement("option");

          option.value = deviceInfo.deviceId;

          if (deviceInfo.kind === "audioinput") {
            option.text = deviceInfo.label || "microphone " + (audioId.length + 1);
            audioId.appendChild(option);
          } else if (deviceInfo.kind === "videoinput") {
            option.text = deviceInfo.label || "camera " + (videoId.length + 1);
            videoId.appendChild(option);
          }
        }
      }

      function getStream() {
        if (window.stream) {
          window.stream.getTracks().forEach(function(track) {
            track.stop();
          });
        }

        const constraints = {
          audio: {
            deviceId: { exact: audioId.value }
          },
          video: {
            deviceId: { exact: videoId.value },
            width: 720,
            height: 720
          }
        };

        navigator.mediaDevices
          .getUserMedia(constraints)
          .then(gotStream)
          .catch(handleError);
      }

      function gotStream(stream) {
        window.stream = stream;
        input.srcObject = stream;
      }

      function handleError(error) {
        console.error("Error: ", error);
      } */