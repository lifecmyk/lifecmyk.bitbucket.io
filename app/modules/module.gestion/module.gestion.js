angular.module("app").component("mGestion", {
  templateUrl: "./app/modules/module.gestion/module.gestion.html",
  controller: (
    $scope,
    server,
    $stateParams,
    $state,
    pdf,
    $http,
    $timeout,
    progress
  ) => {
    info("Module gestion loaded");

    $scope.currentModule = $stateParams.m;
    $scope.progress = progress;

    let moduleRef = server.db.collection($scope.currentModule);

    moduleRef.get().then(snap => {
      $scope.moduleData = [];
      snap.forEach(doc => {
        $scope.$apply(() => {
          $scope.moduleData.push(doc.data());
          $scope.moduleSelected = doc.data().modules[0];
        });

        // update all entrys
        /* let mydoc = doc.data();
        log(mydoc);
        moduleRef.doc(doc.id).update(mydoc).then(() => log("updated")); */
      });
    });

    $scope.edit = item => {
      $state.go("modules", {
        m: `form-${$scope.currentModule}`,
        data: item.ref
      });
    };

    $scope.delete = item => {
      let r = confirm("¿Eliminar permanentemente?");

      if (r === true) {
        progress.start("Eliminando documento");

        server.db
          .collection($scope.currentModule)
          .doc(item.ref)
          .delete()
          .then(() => {
            succ("Document successfully deleted");
            $state.reload();
            $timeout(() => progress.finish());
          })
          .catch(err => {
            warn(`Error removing document: ${err}`);
          });
      }
    };

    app.printGestion($scope, pdf, $http, $timeout, progress);
  }
});
