angular.module("app").component("fGestion", {
  templateUrl: "./app/modules/module.gestion/form.gestion.html",
  controller: (
    $scope,
    server,
    $stateParams,
    $state,
    progress,
    $timeout,
    pdf,
    $http
  ) => {
    info("Form gestion loaded");

    $scope.currentModule = $stateParams.m;
    $scope.parentModule = $scope.currentModule.replace("form-", "");
    $scope.currentData = $stateParams.data;
    $scope.progress = progress;

    //define playout modules relation
    $scope.modules =
      $scope.parentModule === "facturas" ? ["factura"] : ["presupuesto"];
    $scope.months = [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre"
    ];

    if ($scope.currentData === null) {
      //add created data
      let now = new Date();
      let nday = now.getDate();
      let nmonth = now.getMonth();
      let nyear = now.getFullYear();

      $scope.form = {
        _created: now,
        modules: $scope.modules,
        date: {
          day: nday,
          month: nmonth,
          year: nyear
        },
        ret: 7,
        iva: 21,
        to_invoice: []
      };

      //get last number depending on database
      let dbTypeRef = server.db
        .collection($scope.parentModule)
        .orderBy("num", "desc")
        .limit(1);
      dbTypeRef.get().then(snap => {
        snap.forEach(doc => {
          $scope.$apply(() => {
            $scope.form.num = doc.data().num + 1;
          });
        });
      });
    } else {
      let formRef = server.db
        .collection($scope.parentModule)
        .doc($scope.currentData);

      formRef.get().then(doc => {
        $scope.$apply(() => {
          $scope.form = doc.data();
        });

        $scope.sumPrice = $scope.form.to_invoice.reduce((tot, record) => {
          return (
            tot +
            record.price * record.quantity -
            (record.price / 100) * record.discount * record.quantity
          );
        }, 0);

        //check if already is a bill
        $scope.isBill = false;
        let isBillRef = server.db
          .collection("facturas")
          .where("title", "==", $scope.form.title);
        isBillRef.get().then(snap => {
          snap.forEach(doc => {
            if (doc.exists) {
              $scope.$apply(() => ($scope.isBill = true));
            }
          });
        });
      });
    }

    //get list of customers
    let customersRef = server.db.collection($scope.parentModule);
    customersRef.get().then(snap => {
      $scope.customers = [];

      snap.forEach(doc => {
        $scope.customers.push(doc.data());
      });

      const uniqueCustomers = Array.from(
        new Set($scope.customers.map(c => c.to))
      ).map(to => {
        return $scope.customers.find(c => c.to === to);
      });

      $scope.$apply(() => ($scope.customers = uniqueCustomers));
    });

    //auto fill on customer datalist selection
    _("[list='customers']").on("change", e => {
      let autoCustomerRef = server.db
        .collection($scope.parentModule)
        .where("to", "==", e.target.value);

      autoCustomerRef.get().then(snap => {
        snap.forEach(doc => {
          $scope.autoCustomer = doc.data();
        });

        $scope.$apply(() => {
          $scope.form.nif = $scope.autoCustomer.nif;
          $scope.form.address = $scope.autoCustomer.address;
          $scope.form.contact = $scope.autoCustomer.contact;
          e.target.blur();
        });
      });
    });

    //start new line
    $scope.startLine = () => {
      $scope.newLine = true;
    };

    //dicard new line
    $scope.discardLine = () => {
      $scope.newLine = false;
    };

    //add new page
    $scope.line = {};
    $scope.addLine = line => {
      if (line.detail && line.quantity && line.price !== undefined) {
        if (line.discount === undefined) {
          line.discount = 0;
        }

        $scope.form.to_invoice.push(line);
        $scope.line = null;

        $scope.sumPrice = $scope.form.to_invoice.reduce((tot, record) => {
          return (
            tot +
            record.price * record.quantity -
            (record.price / 100) * record.discount * record.quantity
          );
        }, 0);

        $scope.discardLine();
      } else {
        _(".add-line-btn").add("shake");
        $timeout(() => _(".add-line-btn").del("shake"), 1000);
      }
    };

    //delete new page
    $scope.deleteLine = key => {
      $scope.sumPrice = $scope.sumPrice - $scope.form.to_invoice[key].price;
      $scope.form.to_invoice.splice(key, 1);
    };

    //add new
    $scope.add = form => {
      progress.start("Creando documento");

      server.db
        .collection($scope.parentModule)
        .add(form)
        .then(docRef => {
          succ("Data added");

          //def doc id
          let docId = docRef.id;

          server.db
            .collection($scope.parentModule)
            .doc(docId)
            .update({
              ref: docId
            })
            .then(() => {
              succ("Ref added");
              progress.finish();

              $state.go("modules", {
                m: $scope.parentModule,
                data: null
              });
            });
        })
        .catch(err => {
          warn(`Error writing document: ${err}`);
        });
    };

    //update
    $scope.update = form => {
      progress.start("Actualizando documento");

      server.db
        .collection($scope.parentModule)
        .doc($scope.currentData)
        .set(form)
        .then(() => {
          succ("Data updated");
          progress.finish();

          $state.go("modules", {
            m: $scope.parentModule,
            data: null
          });
        })
        .catch(err => {
          warn(`Error writing document: ${err}`);
        });
    };

    //duplicate ppto to bill
    $scope.duplicate = form => {
      //get last number of bills
      let billsRef = server.db
        .collection("facturas")
        .orderBy("num", "desc")
        .limit(1);
      billsRef.get().then(snap => {
        snap.forEach(doc => {
          form.num = doc.data().num + 1;
          form.modules = ["factura"];
          $scope.parentModule = "facturas";

          $scope.add(form);
        });
      });
    };

    $scope.checkPush = form => {
      if ($scope.currentData === null) {
        $scope.add(form);
      } else {
        $scope.update(form);
      }
    };

    app.printGestion($scope, pdf, $http, $timeout, progress);
  }
});
