app.factory("svg", () => {
  let svg = {
    toURL: svgString => {
      return "data:image/svg+xml;utf8," + encodeURIComponent(svgString);
    }
  };

  return svg;
});

app.factory("base64", () => {
  let base64 = {
    toFile: (dataurl, filename) => {
      var arr = dataurl.split(","),
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);
      while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
      }
      return new File([u8arr], filename, { type: "image/jpeg" });
    }
  };

  return base64;
});

app.factory("progress", () => {
  let progress = {
    state: false,
    start: message => {
      progress.state = true;
      progress.msg = message;
    },
    finish: () => (progress.state = false),
    show: message => (progress.msg = message),
    add: num => (progress.part = num),
    msg: "",
    part: 0
  };

  return progress;
});

app.factory("server", () => {
  let server = {
    auth: firebase.auth(),
    db: firebase.firestore(),
    st: firebase.storage(),
    in: (email, pass) => {
      server.auth
        .signInWithEmailAndPassword(email, pass)
        .catch(err => console.error(err.message));
    },
    out: () => {
      server.auth.signOut();
      warn("Disconnected");
    },
    user: {}
  };

  server.auth.onAuthStateChanged(user => {
    if (user) {
      succ("Connected");
      server.user = user;
    } else {
      warn("Disconnected");
      server.user = null;
    }
  });

  return server;
});

app.factory("pdf", () => {
  let toMM = 2.83465;
  let mm = size => size * toMM;

  let pdf = {};
  pdf.mm = size => size * toMM;

  pdf.size = {
    portrait: {
      A0: [mm(864.28), mm(1212.28)],
      A1: [mm(617.28), mm(864.28)],
      A2: [mm(443.28), mm(617.28)],
      SRA3: [mm(343.28), mm(473.28)],
      A3: [mm(320.28), mm(443.28)],
      A4: [mm(233.28), mm(320.28)],
      A5: [mm(171.28), mm(233.28)],
      A6: [mm(128.28), mm(171.28)],
      DL: [mm(223.28), mm(123.28)],
      card: [mm(78.28), mm(108.28)]
    },
    landscape: {
      A0: [mm(1212.28), mm(864.28)],
      A1: [mm(864.28), mm(617.28)],
      A2: [mm(617.28), mm(443.28)],
      SRA3: [mm(473.28), mm(343.28)],
      A3: [mm(443.28), mm(320.28)],
      A4: [mm(320.28), mm(233.28)],
      A5: [mm(233.28), mm(171.28)],
      A6: [mm(171.28), mm(128.28)],
      DL: [mm(123.28), mm(223.28)],
      card: [mm(108.28), mm(78.28)]
    },
    diptic: [mm(233.28), mm(233.28)],
    squareCard: [mm(78.28), mm(78.28)]
  };

  pdf.bleeds = {
    portrait: {
      A0: "./assets/bleeds/portrait-A0.svg",
      A1: "./assets/bleeds/portrait-A1.svg",
      A2: "./assets/bleeds/portrait-A2.svg",
      SRA3: "./assets/bleeds/portrait-SRA3.svg",
      A3: "./assets/bleeds/portrait-A3.svg",
      A4: "./assets/bleeds/portrait-A4.svg",
      A5: "./assets/bleeds/portrait-A5.svg",
      A6: "./assets/bleeds/portrait-A6.svg",
      DL: "./assets/bleeds/portrait-DL.svg",
      card: "./assets/bleeds/portrait-card.svg"
    },
    landscape: {
      A0: "./assets/bleeds/landscape-A0.svg",
      A1: "./assets/bleeds/landscape-A1.svg",
      A2: "./assets/bleeds/landscape-A2.svg",
      SRA3: "./assets/bleeds/landscape-SRA3.svg",
      A3: "./assets/bleeds/landscape-A3.svg",
      A4: "./assets/bleeds/landscape-A4.svg",
      A5: "./assets/bleeds/landscape-A5.svg",
      A6: "./assets/bleeds/landscape-A6.svg",
      DL: "./assets/bleeds/landscape-DL.svg",
      card: "./assets/bleeds/landscape-card.svg"
    },
    diptic: "./assets/bleeds/diptic.svg",
    squareCard: "./assets/bleeds/square-card.svg"
  };

  pdf.margin = {
    big: mm(11.64),
    small: mm(8.64)
  };

  pdf.lorem =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque a turpis mauris. Etiam non pulvinar tortor, a varius magna. Maecenas efficitur, augue varius tempor molestie, mauris sapien suscipit nunc, eget viverra felis erat at elit. Nullam semper laoreet ipsum, sed egestas sapien varius non. Suspendisse potenti. Vestibulum euismod id nunc a posuere. Curabitur volutpat, est nec pharetra iaculis, lectus urna ultricies neque, quis luctus sem nisl vel nibh. Maecenas hendrerit sit amet ipsum porttitor accumsan. Nunc lorem magna, volutpat a lobortis eu, volutpat a mi. Nulla viverra varius quam, vitae fringilla nisi interdum sit amet. Nulla iaculis eu sem sit amet iaculis. Sed sit amet consequat odio, nec lacinia felis. Aliquam erat volutpat. Sed ligula velit, aliquet a facilisis quis, tempor in odio. Nullam et ipsum ac ante fermentum iaculis. Nam sodales ornare tellus, quis vestibulum eros laoreet id. Cras nec eleifend dolor. Nam lobortis felis urna, eget pretium ex feugiat porttitor. Pellentesque tempor porttitor tortor ut egestas. Nullam ornare et ligula quis lacinia. Praesent laoreet magna at nisi ullamcorper facilisis. Duis dapibus, metus aliquet iaculis tincidunt, nibh magna elementum neque, vel iaculis metus dui condimentum augue. Integer convallis vitae ante in ullamcorper. Suspendisse tincidunt fermentum semper. Nulla ornare felis sit amet diam pellentesque euismod. Vivamus aliquam vehicula tincidunt. Curabitur fringilla eu neque quis facilisis. Mauris sagittis quam nec urna ultricies feugiat. Aliquam varius congue sapien, et iaculis diam egestas ac. Quisque consequat nibh vitae urna molestie, vitae gravida orci ultricies. Quisque et vulputate justo. Curabitur quis placerat felis. Nullam eget nisl non risus tempor maximus quis quis arcu. Duis efficitur gravida risus, id consectetur neque lobortis a. Maecenas commodo nec eros vitae venenatis. Etiam porta, ipsum id tempor venenatis, arcu urna eleifend tortor, placerat facilisis est justo sit amet odio. Suspendisse condimentum lacus urna, sed tincidunt est accumsan non. Vivamus sed ultricies ante. Duis eleifend nisl ornare, commodo dolor non, venenatis nisi. In sit amet lorem venenatis, molestie mi eu, maximus nisl. Aenean et erat tortor. Aenean quis urna massa. Phasellus ut tellus vitae nunc convallis faucibus. Donec nunc urna, vehicula eget pharetra et, gravida vitae dui. Nunc pharetra at leo vel molestie. Sed venenatis in quam id accumsan.";

  return pdf;
});
