app.printDossier = ($scope, pdf, $http, $timeout, progress) => {
  $scope.print = (item, m, preview) => {
    if (m === "dossier") {
      info(`PDF starts with module: ${m}`);
      progress.add(0);
      progress.start("Creando PDF");

      //def pdf name
      const pdfName = `${item.name}_DOSSIER`;

      //milimeters function
      const mm = num => pdf.mm(num);

      //pdf options
      const doc = new PDFDocument({
        size: [mm(297), mm(210)],
        margin: 0,
        info: {
          Title: pdfName
        }
      });

      //title line counter
      const lineTitle = ({
        lineChar,
        text,
        width,
        height,
        lineX,
        oneLineY,
        twoLinesY,
        threeLinesY,
        lineSpace
      }) => {
        //una linea
        if (text.length <= lineChar) {
          doc
            .font("title", 35)
            .fillColor("#fff")
            .text(text, mm(lineX), mm(oneLineY), {
              width: mm(width),
              height: mm(height)
            });
          //2 lineas
        } else if (text.length <= lineChar * 2) {
          doc
            .font("title", 35)
            .fillColor("#fff")
            .text(text, mm(lineX), mm(twoLinesY), {
              width: mm(width),
              height: mm(height),
              lineGap: lineSpace
            });
          //3 lineas
        } else if (text.length <= lineChar * 3) {
          doc
            .font("title", 35)
            .fillColor("#fff")
            .text(text, mm(lineX), mm(threeLinesY), {
              width: mm(width),
              height: mm(height),
              lineGap: lineSpace
            });
          //4 lineas
        }
      };

      //generate blob from pdf
      let stream = doc.pipe(blobStream());

      // preload files
      async function asyncData() {
        //def font types
        let title;
        let base;

        // load fonts
        title = await $http.get(
          `./assets/templates/dossier/cmyk/Circular-Black.ttf`,
          {
            responseType: "arraybuffer"
          }
        );
        base = await $http.get(
          `./assets/templates/dossier/cmyk/Lora-Italic.ttf`,
          {
            responseType: "arraybuffer"
          }
        );

        // register fonts
        doc.registerFont("title", title.data);
        doc.registerFont("base", base.data);

        // bleeds
        /* $scope.bleeds = await $http.get(pdf.bleeds.landscape.A4); */

        // images & vectors
        $scope.portada = await $http.get(
          `./assets/templates/dossier/portada.svg`
        );
        $scope.contra = await $http.get(
          `./assets/templates/dossier/contra.svg`
        );
        $scope.fullImg = await $http.get(
          `./assets/templates/dossier/full-img.svg`
        );
        $scope.leftTxtImg = await $http.get(
          `./assets/templates/dossier/left-txt-img.svg`
        );
        $scope.rightTxtImg = await $http.get(
          `./assets/templates/dossier/right-txt-img.svg`
        );
        $scope.fullTxt = await $http.get(
          `./assets/templates/dossier/full-txt.svg`
        );
        $scope.dblImg = await $http.get(
          `./assets/templates/dossier/dbl-img.svg`
        );
        $scope.fullHphoto = await $http.get(
          `./assets/templates/dossier/full-hphoto.svg`
        );
        $scope.dblVphoto = await $http.get(
          `./assets/templates/dossier/dbl-vphoto.svg`
        );
        $scope.fullVphoto = await $http.get(
          `./assets/templates/dossier/full-vphoto.svg`
        );
        $scope.leftTxtHphoto = await $http.get(
          `./assets/templates/dossier/left-txt-hphoto.svg`
        );
        $scope.rightTxtHphoto = await $http.get(
          `./assets/templates/dossier/right-txt-hphoto.svg`
        );
        $scope.leftTxtVphoto = await $http.get(
          `./assets/templates/dossier/left-txt-vphoto.svg`
        );
        $scope.rightTxtVphoto = await $http.get(
          `./assets/templates/dossier/right-txt-vphoto.svg`
        );

        //images from database
        let pages = item.pages;
        let pageObj = {};
        $scope.imageObj = {};

        $scope.dbImages = async () => {
          for (let p = 0; p < pages.length; p++) {
            pageObj[`page${p}`] = pages[p];

            //increess progress bar for each page
            progress.show(`Creando página ${p + 1}`);
            progress.add(((p + 1) * 100) / pages.length - 5);

            //check if page has images
            if (pageObj[`page${p}`].images) {
              for (let i = 0; i < pageObj[`page${p}`].images.length; i++) {
                $scope.imageObj[`page${p}Image${i}`] = await $http.get(
                  pageObj[`page${p}`].images[i],
                  { responseType: "arraybuffer" }
                );

                //on all images loaded change progress bar to 100
                if (p === pages.length - 1) {
                  $timeout(() => {
                    progress.show("Procesando PDF...");
                    progress.add(100);
                  });
                }
              }
            }
          }
        };
      }

      // await for static resources
      asyncData().then(() => {
        // await for db resources
        $scope
          .dbImages()
          .then(() => {
            //design pdf
            let pages = item.pages;

            //portada
            doc.addSVG($scope.portada.data, 0, 0, {
              width: mm(297),
              height: mm(210)
            });

            lineTitle({
              lineChar: 14,
              text: item.name,
              width: 80,
              height: 80,
              lineX: 170,
              oneLineY: 95,
              twoLinesY: 88,
              threeLinesY: 82,
              lineSpace: null
            });

            /* let portadaSh = mm(
              doc.heightOfString(item.name, { width: mm(80) })
            );
            let pageH = mm(210);
            let portadaSp = (pageH - portadaSh) / 2;
            log(portadaSh, portadaSp);

            doc
              .font("title", 35)
              .fillColor("#fff")
              .text(item.name, mm(170), portadaSp - 5, {
                width: mm(80)
              }); */

            for (let p = 0; p < pages.length; p++) {
              doc.addPage();

              //full-img
              if (pages[p].type === "full-img") {
                doc
                  .save()
                  .rect(mm(10), mm(10), mm(277), mm(190))
                  .clip()
                  .image(
                    $scope.imageObj[`page${p}Image${0}`].data,
                    mm(10),
                    mm(10),
                    {
                      cover: [mm(277), mm(190)],
                      align: "center",
                      valign: "center"
                    }
                  )
                  .restore();

                doc.addSVG($scope.fullImg.data, 0, 0, {
                  width: mm(297),
                  height: mm(210)
                });
              }

              //left-txt-img
              if (pages[p].type === "left-txt-img") {
                //image
                doc
                  .save()
                  .rect(mm(102), mm(10), mm(184), mm(189.5))
                  .clip()
                  .image(
                    $scope.imageObj[`page${p}Image${0}`].data,
                    mm(103.5),
                    mm(10),
                    {
                      cover: [mm(184), mm(189.5)],
                      align: "center",
                      valign: "center"
                    }
                  )
                  .restore();

                //vectors
                doc.addSVG($scope.leftTxtImg.data, 0, 0, {
                  width: mm(297),
                  height: mm(210)
                });

                //title
                lineTitle({
                  lineChar: 12,
                  text: item.pages[p].title,
                  width: 70,
                  height: 40,
                  lineX: 20,
                  oneLineY: 65,
                  twoLinesY: 53,
                  threeLinesY: null,
                  lineSpace: -5
                });

                //text
                doc
                  .font("base", 11)
                  .fillColor("#fff")
                  .text(item.pages[p].text, mm(20), mm(100), {
                    width: mm(70),
                    height: mm(150)
                  });
              }

              //right-txt-img
              if (pages[p].type === "right-txt-img") {
                //image
                doc
                  .save()
                  .rect(mm(10.7), mm(10), mm(184), mm(189.5))
                  .clip()
                  .image(
                    $scope.imageObj[`page${p}Image${0}`].data,
                    mm(10.7),
                    mm(10),
                    {
                      cover: [mm(184), mm(189.5)],
                      align: "center",
                      valign: "center"
                    }
                  )
                  .restore();

                //vectors
                doc.addSVG($scope.rightTxtImg.data, 0, 0, {
                  width: mm(297),
                  height: mm(210)
                });

                //title
                lineTitle({
                  lineChar: 12,
                  text: item.pages[p].title,
                  width: 70,
                  height: 40,
                  lineX: 207,
                  oneLineY: 65,
                  twoLinesY: 53,
                  threeLinesY: null,
                  lineSpace: -5
                });

                //text
                doc
                  .font("base", 11)
                  .fillColor("#fff")
                  .text(item.pages[p].text, mm(207), mm(100), {
                    width: mm(70),
                    height: mm(150)
                  });
              }

              //full-txt
              if (pages[p].type === "full-txt") {
                doc.addSVG($scope.fullTxt.data, 0, 0, {
                  width: mm(297),
                  height: mm(210)
                });

                lineTitle({
                  lineChar: 12,
                  text: item.pages[p].title,
                  width: 80,
                  height: 40,
                  lineX: 45,
                  oneLineY: 62,
                  twoLinesY: 50,
                  threeLinesY: null,
                  lineSpace: -5
                });

                doc
                  .font("base", 11)
                  .fillColor("#fff")
                  .text(item.pages[p].text, mm(45), mm(100), {
                    width: mm(210),
                    height: mm(60),
                    columns: 2,
                    columnGap: mm(10)
                  });
              }

              //dbl-img
              if (pages[p].type === "dbl-img") {
                doc
                  .save()
                  .rect(mm(10), mm(10), mm(138.5), mm(190))
                  .clip()
                  .image(
                    $scope.imageObj[`page${p}Image${0}`].data,
                    mm(10),
                    mm(10),
                    {
                      cover: [mm(138.5), mm(190)],
                      align: "center",
                      valign: "center"
                    }
                  )
                  .restore();

                doc
                  .save()
                  .rect(mm(148.5), mm(10), mm(138.5), mm(190))
                  .clip()
                  .image(
                    $scope.imageObj[`page${p}Image${1}`].data,
                    mm(148.5),
                    mm(10),
                    {
                      cover: [mm(138.5), mm(190)],
                      align: "center",
                      valign: "center"
                    }
                  )
                  .restore();

                doc.addSVG($scope.dblImg.data, 0, 0, {
                  width: mm(297),
                  height: mm(210)
                });
              }

              //full-hphoto
              if (pages[p].type === "full-hphoto") {
                doc.addSVG($scope.fullHphoto.data, 0, 0, {
                  width: mm(297),
                  height: mm(210)
                });

                doc
                  .save()
                  .rect(mm(10), mm(25), mm(240), mm(160))
                  .clip()
                  .image(
                    $scope.imageObj[`page${p}Image${0}`].data,
                    mm(10),
                    mm(25),
                    {
                      cover: [mm(240), mm(160)],
                      align: "center",
                      valign: "center"
                    }
                  )
                  .restore();
              }

              //dbl-vphoto
              if (pages[p].type === "dbl-vphoto") {
                doc.addSVG($scope.dblVphoto.data, 0, 0, {
                  width: mm(297),
                  height: mm(210)
                });

                doc
                  .save()
                  .rect(mm(28), mm(10), mm(116), mm(174))
                  .clip()
                  .image(
                    $scope.imageObj[`page${p}Image${0}`].data,
                    mm(28),
                    mm(10),
                    {
                      cover: [mm(116), mm(174)],
                      align: "center",
                      valign: "center"
                    }
                  )
                  .restore();

                doc
                  .save()
                  .rect(mm(153), mm(10), mm(116), mm(174))
                  .clip()
                  .image(
                    $scope.imageObj[`page${p}Image${1}`].data,
                    mm(153),
                    mm(10),
                    {
                      cover: [mm(116), mm(174)],
                      align: "center",
                      valign: "center"
                    }
                  )
                  .restore();
              }

              //full-vphoto
              if (pages[p].type === "full-vphoto") {
                doc.addSVG($scope.fullVphoto.data, 0, 0, {
                  width: mm(297),
                  height: mm(210)
                });

                doc
                  .save()
                  .rect(mm(85.16), mm(10), mm(126.667), mm(190))
                  .clip()
                  .image(
                    $scope.imageObj[`page${p}Image${0}`].data,
                    mm(85.16),
                    mm(10),
                    {
                      cover: [mm(126.667), mm(190)],
                      align: "center",
                      valign: "center"
                    }
                  )
                  .restore();
              }

              //left-txt-hphoto
              if (pages[p].type === "left-txt-hphoto") {
                //vectors
                doc.addSVG($scope.leftTxtHphoto.data, 0, 0, {
                  width: mm(297),
                  height: mm(210)
                });

                //image
                doc
                  .save()
                  .rect(mm(104), mm(44), mm(183), mm(122))
                  .clip()
                  .image(
                    $scope.imageObj[`page${p}Image${0}`].data,
                    mm(104),
                    mm(44),
                    {
                      cover: [mm(183), mm(122)],
                      align: "center",
                      valign: "center"
                    }
                  )
                  .restore();

                //title
                lineTitle({
                  lineChar: 12,
                  text: item.pages[p].title,
                  width: 74,
                  height: 40,
                  lineX: 20,
                  oneLineY: 65,
                  twoLinesY: 53,
                  threeLinesY: null,
                  lineSpace: -5
                });

                //text
                doc
                  .font("base", 11)
                  .fillColor("#fff")
                  .text(item.pages[p].text, mm(20), mm(100), {
                    width: mm(74),
                    height: mm(150)
                  });
              }

              //right-txt-hphoto
              if (pages[p].type === "right-txt-hphoto") {
                //vectors
                doc.addSVG($scope.rightTxtHphoto.data, 0, 0, {
                  width: mm(297),
                  height: mm(210)
                });

                //image
                doc
                  .save()
                  .rect(mm(10), mm(44), mm(183), mm(122))
                  .clip()
                  .image(
                    $scope.imageObj[`page${p}Image${0}`].data,
                    mm(10),
                    mm(44),
                    {
                      cover: [mm(183), mm(122)],
                      align: "center",
                      valign: "center"
                    }
                  )
                  .restore();

                //title
                lineTitle({
                  lineChar: 12,
                  text: item.pages[p].title,
                  width: 74,
                  height: 40,
                  lineX: 203,
                  oneLineY: 65,
                  twoLinesY: 53,
                  threeLinesY: null,
                  lineSpace: -5
                });

                //text
                doc
                  .font("base", 11)
                  .fillColor("#fff")
                  .text(item.pages[p].text, mm(203), mm(100), {
                    width: mm(74),
                    height: mm(150)
                  });
              }

              //left-txt-vphoto
              if (pages[p].type === "left-txt-vphoto") {
                //vectors
                doc.addSVG($scope.leftTxtVphoto.data, 0, 0, {
                  width: mm(297),
                  height: mm(210)
                });

                //image
                doc
                  .save()
                  .rect(mm(130), mm(10), mm(126.667), mm(190))
                  .clip()
                  .image(
                    $scope.imageObj[`page${p}Image${0}`].data,
                    mm(130),
                    mm(10),
                    {
                      cover: [mm(126.667), mm(190)],
                      align: "center",
                      valign: "center"
                    }
                  )
                  .restore();

                //title
                lineTitle({
                  lineChar: 12,
                  text: item.pages[p].title,
                  width: 70,
                  height: 40,
                  lineX: 30,
                  oneLineY: 65,
                  twoLinesY: 53,
                  threeLinesY: null,
                  lineSpace: -5
                });

                //text
                doc
                  .font("base", 11)
                  .fillColor("#fff")
                  .text(item.pages[p].text, mm(30), mm(100), {
                    width: mm(70),
                    height: mm(150)
                  });
              }

              //right-txt-vphoto
              if (pages[p].type === "right-txt-vphoto") {
                //vectors
                doc.addSVG($scope.rightTxtVphoto.data, 0, 0, {
                  width: mm(297),
                  height: mm(210)
                });

                //image
                doc
                  .save()
                  .rect(mm(40), mm(10), mm(126.667), mm(190))
                  .clip()
                  .image(
                    $scope.imageObj[`page${p}Image${0}`].data,
                    mm(40),
                    mm(10),
                    {
                      cover: [mm(126.667), mm(190)],
                      align: "center",
                      valign: "center"
                    }
                  )
                  .restore();

                //title
                lineTitle({
                  lineChar: 12,
                  text: item.pages[p].title,
                  width: 70,
                  height: 40,
                  lineX: 194,
                  oneLineY: 65,
                  twoLinesY: 53,
                  threeLinesY: null,
                  lineSpace: -5
                });

                //text
                doc
                  .font("base", 11)
                  .fillColor("#fff")
                  .text(item.pages[p].text, mm(194), mm(100), {
                    width: mm(70),
                    height: mm(150)
                  });
              }
            }

            doc.addPage();

            doc.addSVG($scope.contra.data, 0, 0, {
              width: mm(297),
              height: mm(210)
            });

            // render pdf
            doc.end();
          })
          .catch(err => {
            alert(
              `Algo ha salido mal. Vulve a intentarlo o ponte en contacto con el desarrollador.\n\n${err}`
            );
            location.reload();
          });
      });

      // get pdf
      stream.on("finish", () => {
        let blob = stream.toBlobURL("application/pdf");

        preview = preview === undefined ? false : preview;

        if (preview) {
          let frame = document.querySelector("iframe");
          frame.classList.add("dev-active");
          frame.src = blob;
        } else {
          let link = document.createElement("a");
          link.href = blob;
          link.download = `${pdfName}.pdf`;
          link.click();
        }

        info("PDF ends");
        progress.finish();

        $timeout(() => {
          window.URL.revokeObjectURL(blob);
        }, 2000);

        /* let buffer = stream.toBlob("application/pdf");

        function blobToDataURL(blob, callback) {
          var a = new FileReader();
          a.onload = function(e) {
            callback(e.target.result);
          };
          a.readAsDataURL(blob);
        }

        blobToDataURL(buffer, function(url) {
          log(url);
        }); */
      });
    } else {
      alert(`El modulo ${m} no esta disponible`);
    }
  };
};


//# sourceMappingURL=print.dossier.js.map