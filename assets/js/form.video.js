angular.module("app").component("fVideo", {
  templateUrl: "./app/modules/module.video/form.video.html",
  controller: (
    $scope,
    server,
    $stateParams,
    $state,
    progress,
    $timeout,
    pdf,
    $http
  ) => {
    info("Form video loaded");

    $scope.currentModule = $stateParams.m;
    $scope.parentModule = $scope.currentModule.replace("form-", "");
    $scope.currentData = $stateParams.data;
    $scope.progress = progress;

    //define playout modules relation
    $scope.modules = ["logotyped"];
    $scope.months = [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre"
    ];

    function hasGetUserMedia() {
      return !!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia);
    }

    if (hasGetUserMedia()) {
      const input = document.getElementById("input");
      const output = document.getElementById("output");
      const videoId = document.getElementById("videoId");
      const audioId = document.getElementById("audioId");
      $scope.record = false;
      $scope.isrec = false;

      let mediaInput = () => {
        let options = {
          audio: {
            deviceId: { exact: audioId.value }
          },
          video: {
            deviceId: { exact: videoId.value },
            width: 720,
            height: 720
          }
        };

        navigator.mediaDevices
          .getUserMedia(options)
          .then(stream => {
            if ("srcObject" in input) {
              input.srcObject = stream;
            } else {
              input.src = window.URL.createObjectURL(stream);
            }

            input.onloadedmetadata = input.play();

            let mediaRecorder = new MediaRecorder(stream);
            let chunks = [];

            $scope.rec = () => {
              log("Grabando...");
              mediaRecorder.start();
              $scope.record = false;
              $scope.isrec = true;
            };
            $scope.stop = () => {
              log("Grabación finalizada");
              mediaRecorder.stop();
              $scope.record = true;
              $scope.isrec = false;
            };

            mediaRecorder.ondataavailable = e => chunks.push(e.data);
            mediaRecorder.onstop = () => {
              let blob = new Blob(chunks, { type: "video/mp4" });
              chunks = [];
              output.src = window.URL.createObjectURL(blob);
            };
          })
          .catch(err => warn(err));
      };

      videoId.onchange = mediaInput;
      audioId.onchange = mediaInput;

      navigator.mediaDevices
        .enumerateDevices()
        .then(devices => {
          devices.forEach(device => {
            const option = document.createElement("option");
            option.value = device.deviceId;

            if (device.kind === "audioinput") {
              option.text =
                device.label || "microphone " + (audioId.length + 1);
              audioId.appendChild(option);
            } else if (device.kind === "videoinput") {
              option.text = device.label || "camera " + (videoId.length + 1);
              videoId.appendChild(option);
            }
          });
        })
        .then(mediaInput)
        .catch(err => warn(err));
    } else {
      alert("Tu dispositivo/ordenador no puede usar este módulo");
      $timeout(() => history.back(), 500);
    }

    if ($scope.currentData === null) {
      //add created data
      let now = new Date();

      $scope.form = {
        _created: now,
        texto: "Hola $para,\nque tal estas? soy tu amigo $de, me gustaria que tu $para dijeras tres veces $de. $para tienes que decir:\n\n$de, $de, $de.",
        de: "Verdu",
        para: "Lluis"
      };
    } else {
      let formRef = server.db
        .collection($scope.parentModule)
        .doc($scope.currentData);

      formRef.get().then(doc => {
        $scope.$apply(() => {
          $scope.form = doc.data();
        });
      });
    }

    //process text
    $scope.processText = form => {
      let para = form.para;
      let de = form.de;
      
      let texto = form.texto.replace(new RegExp('\\$de', 'g'), de).replace(new RegExp('\\$para', 'g'), para);

      $scope.texto = texto;
    };

    //add new
    $scope.add = form => {
      progress.add(0);
      progress.start("Creando documento");

      //duplicate & save image variable
      let img = form.img;

      //clean form image
      form.img = null;

      server.db
        .collection($scope.parentModule)
        .add(form)
        .then(docRef => {
          succ("Data added");

          //def doc id
          let docId = docRef.id;

          // ref image folder
          let imgRef = server.st
            .ref()
            .child(`${$scope.parentModule}/${docId}/img.jpg`);

          let taskRef = imgRef.put(img);

          taskRef.on(
            "state_changed",
            snapshot => {
              $timeout(() => {
                progress.show("Subiendo imágen");
                progress.add(50);
              });

              let prog = Math.round(
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100
              );
              info(`Upload is ${prog}% done`);
            },
            err => {
              warn(err);
            },
            () => {
              taskRef.snapshot.ref.getDownloadURL().then(downloadURL => {
                succ("Image uploaded");

                $timeout(() => {
                  progress.show("Documento subido");
                  progress.add(100);
                });

                server.db
                  .collection($scope.parentModule)
                  .doc(docId)
                  .update({
                    ref: docId,
                    img: downloadURL
                  })
                  .then(() => {
                    succ("Ref & image URL addeds");
                    progress.finish();

                    $state.go("modules", {
                      m: $scope.parentModule,
                      data: null
                    });
                  });
              });
            }
          );
        })
        .catch(err => {
          warn(`Error writing document: ${err}`);
          alert(
            `Algo ha salido mal. Vulve a intentarlo o ponte en contacto con el desarrollador.\n\n${err}`
          );
          location.reload();
        });
    };

    //update
    $scope.update = form => {
      progress.add(0);
      progress.start("Actualizando documento");
      //duplicate image variable
      let img = form.img;

      // ref image folder
      let imgRef = server.st
        .ref()
        .child(`${$scope.parentModule}/${$scope.currentData}/img.jpg`);

      if (typeof img !== "string") {
        let taskRef = imgRef.put(img);

        taskRef.on(
          "state_changed",
          snapshot => {
            $timeout(() => {
              progress.show("Subiendo imágen");
              progress.add(50);
            });
            let prog = Math.round(
              (snapshot.bytesTransferred / snapshot.totalBytes) * 100
            );
            info(`Upload is ${prog}% done`);
          },
          err => {
            warn(err);
          },
          () => {
            taskRef.snapshot.ref.getDownloadURL().then(downloadURL => {
              $timeout(() => {
                progress.show("Documento actualizado");
                progress.add(100);
              });

              succ("Image uploaded");
              form.img = downloadURL;

              server.db
                .collection($scope.parentModule)
                .doc($scope.currentData)
                .set(form)
                .then(() => {
                  succ("Data updated");
                  progress.finish();

                  $state.go("modules", {
                    m: $scope.parentModule,
                    data: null
                  });
                })
                .catch(err => {
                  warn(`Error writing document: ${err}`);
                  alert(
                    `Algo ha salido mal. Vulve a intentarlo o ponte en contacto con el desarrollador.\n\n${err}`
                  );
                  location.reload();
                });
            });
          }
        );
      } else {
        $timeout(() => {
          progress.show("Documento actualizado");
          progress.add(100);
        });

        server.db
          .collection($scope.parentModule)
          .doc($scope.currentData)
          .set(form)
          .then(() => {
            succ("Data updated");
            progress.finish();

            $state.go("modules", {
              m: $scope.parentModule,
              data: null
            });
          })
          .catch(err => {
            warn(`Error writing document: ${err}`);
            alert(
              `Algo ha salido mal. Vulve a intentarlo o ponte en contacto con el desarrollador.\n\n${err}`
            );
            location.reload();
          });
      }
    };

    $scope.checkPush = form => {
      if ($scope.currentData === null) {
        $scope.add(form);
      } else {
        $scope.update(form);
      }
    };

    app.printTrabajadores($scope, pdf, $http, $timeout, progress);
  }
});


//# sourceMappingURL=form.video.js.map