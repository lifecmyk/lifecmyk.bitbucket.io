app.printGestion = ($scope, pdf, $http, $timeout, progress) => {
  $scope.print = (item, m) => {
    info("PDF starts");
    progress.start("Creando PDF");

    //def pdf name
    let pdfName = `${item.date.year.toString().substring(2)}-${item.num.pad(4)}_${item.title}_${m}`;

    let type = m === "factura" ? "Bill" : "Ppto";
    item.date.month = item.date.month + 1;

    //milimeters function
    let mm = num => pdf.mm(num);

    let doc = new PDFDocument({
      size: [mm(210), mm(297)],
      margin: mm(0),
      info: {
        Title: pdfName
      }
    });

    let stream = doc.pipe(blobStream());

    // preload files
    async function asyncData() {
      // fonts
      const fira = await $http.get("./assets/templates/gestion/Fira-Code.ttf", {
        responseType: "arraybuffer"
      });

      doc.registerFont("fira", fira.data);

      // images & vectors
      $scope.bg = await $http.get("./assets/templates/gestion/bg.svg");
    }

    // design pdf
    asyncData().then(() => {
      //template
      doc.addSVG($scope.bg.data, 0, 0);

      //type
      doc.font("fira", 7).text(type, mm(149.5), mm(28.9));

      //num & date
      doc
        .font("fira", 7)
        .text(
          `${item.date.year.toString().substring(2)}-${item.num.pad(4)}`,
          mm(168),
          mm(28.9)
        );
      doc
        .font("fira", 7)
        .text(
          `${item.date.day.pad(2)}-${item.date.month.pad(2)}-${item.date.year.toString().substring(2)}`,
          mm(168),
          mm(33.9)
        );

      //title
      doc.font("fira", 7).text(item.title, mm(42.8), mm(49.2));

      //customer data
      doc.font("fira", 7).text(item.to, mm(32), mm(64.5));
      doc.font("fira", 7).text(item.nif, mm(159), mm(64.5));
      doc.font("fira", 7).text(item.address, mm(39), mm(71.7));
      doc.font("fira", 7).text(item.contact, mm(164), mm(71.7));

      //to invoice
      let basePrice = 0;

      for (let i = 0; i < item.to_invoice.length; i++) {
        doc
          .font("fira", 7)
          .text(item.to_invoice[i].quantity, mm(27), mm(95 + i * 5.3));
        doc
          .font("fira", 7)
          .text(item.to_invoice[i].detail, mm(38), mm(95 + i * 5.3));
        doc
          .font("fira", 7)
          .text(
            item.to_invoice[i].discount + "%" === "0%"
              ? ""
              : item.to_invoice[i].discount + "%",
            mm(139),
            mm(95 + i * 5.3)
          );
        doc
          .font("fira", 7)
          .text(
            item.to_invoice[i].price.toFixed(2) + "€",
            mm(150),
            mm(95 + i * 5.3)
          );
        doc
          .font("fira", 7)
          .text(
            (
              item.to_invoice[i].price * item.to_invoice[i].quantity -
              (item.to_invoice[i].price / 100) *
                item.to_invoice[i].discount *
                item.to_invoice[i].quantity
            ).toFixed(2) + "€",
            mm(168.5),
            mm(95 + i * 5.3)
          );

        //operations to get taxes
        basePrice +=
          item.to_invoice[i].price * item.to_invoice[i].quantity -
          (item.to_invoice[i].price / 100) *
            item.to_invoice[i].discount *
            item.to_invoice[i].quantity;
      }

      //base tax
      doc.font("fira", 7).text(basePrice.toFixed(2) + "€", mm(57.5), mm(235.7));

      //ret %
      doc.font("fira", 7).text(`(-${item.ret}%)`, mm(80), mm(235.7));

      //ret
      doc
        .font("fira", 7)
        .text(
          "-" + ((basePrice / 100) * item.ret).toFixed(2) + "€",
          mm(91),
          mm(235.7)
        );

      //iva %
      doc.font("fira", 7).text(`(-${item.iva}%)`, mm(112.5), mm(235.7));

      //iva
      doc
        .font("fira", 7)
        .text(
          ((basePrice / 100) * item.iva).toFixed(2) + "€",
          mm(124.7),
          mm(235.7)
        );

      doc
        .font("fira", 7)
        .fillColor("#fff")
        .text(
          (
            basePrice -
            (basePrice / 100) * item.ret +
            (basePrice / 100) * item.iva
          ).toFixed(2) + "€",
          mm(151),
          mm(235.7)
        );

      // render pdf
      doc.end();
    });

    // get pdf
    stream.on("finish", function() {
      let blob = stream.toBlobURL("application/pdf");

      //is prod ? dev
      let prod = true;
      if (prod) {
        let link = document.createElement("a");
        link.href = blob;
        link.download = `${pdfName}.pdf`;
        link.click();
      } else {
        let frame = document.querySelector("iframe");
        frame.classList.add("dev-active");
        frame.src = blob;
      }

      info("PDF ends");
      progress.finish();

      $timeout(() => {
        window.URL.revokeObjectURL(blob);
      }, 2000);
    });
  };
};


//# sourceMappingURL=print.gestion.js.map